const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  content: ["./pages/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx}"],
  theme: {
    fontFamily: {
      sans: ["Poppins", "sans-serif"],
    },
    extend: {
      colors: {
        primary: "#1FC1C3",
        secondary: "#298EFF",
        footer: "#091F31",
        auth: "#222222",
        navy: "#0A253B",
        basic: "#171717",
        subTitle: "#888888",
        mainContent: "#F7F8F9",
        adminContent: "#166A7E",
      },
      transitionProperty: {
        width: "width",
        spacing: "margin, padding",
      },
      fontFamily: {
        sans: ["Poppins", ...defaultTheme.fontFamily.sans],
      },
    },
  },
  important: "#tailwind-selector",
};
