import { http } from "../utils/http";
import useSWR from "swr";

const url = {
  transaction: ({ page, pageSize }) => `/transaction/?page=${page}&pageSize=${pageSize}`,
  createTransaction: (idKos) => `/transaction/${idKos}`,
  detail: (param) => `/transaction/${param}`,
  myTransaction: () => `/transaction/all/my-order`,
  myTransactionDetail: (id) => `/transaction/all/my-order/${id}`,
  OwnerTransaction: () => `/transaction/approval/my-kos`,
  approveTransaction: (id) => `/transaction/approve/${id}`,
  rejectTransaction: (id) => `/transaction/reject/${id}`,
  deleteTransaction: (id) => `/transaction/${id}/`,
  recentTransaction: ({ page, pageSize }) => `/transaction/recent/tr?page=${page}&pageSize=${pageSize}`,
};

const hooks = {
  getAllTransaction(filter) {
    return useSWR(url.transaction(filter), http.fetcher);
  },
  getDetail(id) {
    return useSWR(url.detail(id), http.fetcher);
  },
  getMyTransaction() {
    return useSWR(url.myTransaction(), http.fetcher);
  },
  getMyTransactionDetail(id) {
    return useSWR(url.myTransactionDetail(id), http.fetcher);
  },
  getOwnerTrasaction() {
    return useSWR(url.OwnerTransaction(), http.fetcher);
  },
  getRecentTransaction(filter) {
    return useSWR(url.recentTransaction(filter), http.fetcher);
  },
};

const manipulateData = {
  createTransaction(idKos, data) {
    return http.post(url.createTransaction(idKos)).send(data);
  },
  uploadProductImage(data) {
    return http.upload(url.uploadProductImage(), data);
  },
  createPayment(idTransaction, data) {
    return http.put(url.detail(idTransaction)).send(data);
  },
  approveStatus(id) {
    return http.put(url.approveTransaction(id)).send();
  },
  rejectStatus(id) {
    return http.put(url.rejectTransaction(id)).send();
  },
  cancelTransaction(id) {
    return http.del(url.deleteTransaction(id)).send();
  },
};

export const TransactionRepository = {
  url,
  manipulateData,
  hooks,
};
