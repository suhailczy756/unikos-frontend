import { http } from "../utils/http";
import useSWR from "swr";

const url = {
  kost: ({ page, pageSize }) => `/kos?page=${page}&pageSize=${pageSize}`,
  updatekos: (id) => `/kos/editkos/${id}`,
  detailKost: (id) => `/kos/detail/${id}`,
  searchKos: ({ page, pageSize, province, city, district, village }) => `/kos/search/search?page=${page}&pageSize=${pageSize}&province=${province}&city=${city}&district=${district}&village=${village}`,
  createKost: () => `/kos/`,
  allKos: () => `/kos/allkos`,
  allFacility: () => `/facility`,
  createFacility: () => `/facility`,
  requestKos: ({ status, page, pageSize }) => `/kos/status/${status}?page=${page}&pageSize=${pageSize}`,
  highestKos: ({ page, pageSize }) => `/kos/status/tersedia?page=${page}&pageSize=${pageSize}`,
  facility: () => `/facility/type/type`,
  province: () => `/location/province`,
  city: (id) => `/location/city/${id}`,
  allCity: () => `/location/city`,
  district: (id) => `/location/district/${id}`,
  village: (id) => `/location/village/${id}`,
  approval: ({ approval, id }) => `/kos/button/${approval}/${id}`,
  review: (id) => `/review/${id}`,
  recomend: () => `/kos/recomend`,
  deleteFacility: (id) => `/facility/${id}`,
  kosByCity: ({ city }) => `/kos/city/${city}`,
};

const hooks = {
  useKost(filter) {
    return useSWR(url.kost(filter), http.fetcher);
  },
  useDetailKost(filter) {
    return useSWR(url.detailKost(filter), http.fetcher);
  },
  getAllKos() {
    return useSWR(url.allKos(), http.fetcher);
  },
  getAllFacility() {
    return useSWR(url.allFacility(), http.fetcher);
  },
  useRequestKos(filter) {
    return useSWR(url.requestKos(filter), http.fetcher);
  },

  getKosTersedia(filter) {
    return useSWR(url.highestKos(filter), http.fetcher);
  },
  getFacility() {
    return useSWR(url.facility(), http.fetcher);
  },
  getProvince() {
    return useSWR(url.province(), http.fetcher);
  },

  getKosByCity(filter) {
    return useSWR(url.kosByCity(filter), http.fetcher);
  },
  getCity(filter) {
    return useSWR(url.city(filter), http.fetcher);
  },
  getAllCity() {
    return useSWR(url.allCity(), http.fetcher);
  },
  getDistrict(filter) {
    return useSWR(url.district(filter), http.fetcher);
  },
  getVillage(filter) {
    return useSWR(url.village(filter), http.fetcher);
  },
  useSearchKos(filter) {
    return useSWR(url.searchKos(filter), http.fetcher);
  },
  useRecomend() {
    return useSWR(url.recomend(), http.fetcher);
  },
};

const manipulateData = {
  createKos(data) {
    return http.post(url.createKost()).send(data);
  },
  updateKos(id, data) {
    return http.put(url.updatekos(id)).send(data);
  },
  uploadProductImage(data) {
    return http.upload(url.uploadProductImage(), data);
  },
  createFacility(data) {
    return http.post(url.createFacility()).send(data);
  },
  updateStatus(data) {
    return http.put(url.approval(data));
  },
  createReview(id, value) {
    return http.post(url.review(id)).send(value);
  },
  deleteFacility(id) {
    return http.del(url.deleteFacility(id));
  },
};

export const kosRepository = {
  url,
  manipulateData,
  hooks,
};
