import useSWR from "swr";
import { http } from "../utils/http";
const url = {
  image: () => `/upload/users`,
  kos: () => `/file/upload`,
};
const hooks = {
  useImage(filter) {
    return useSWR(url.image(filter), http.fetcher);
  },
  useVenue(filter) {
    return useSWR(url.kos(filter), http.fetcher);
  },
};
const manipulateData = {
  editImage(data) {
    return http.post(url.image()).send(data);
  },
  upload(file) {
    const formData = new FormData();
    formData.append("user", file);
    return http.post(url.image()).send(formData);
  },
  upKos(file) {
    const formData = new FormData();
    formData.append("file", file);
    return http.post(url.kos()).send(formData);
  },
};
export const imageRepository = {
  url,
  manipulateData,
  hooks,
};
