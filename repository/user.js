import { http } from "../utils/http";
import useSWR from "swr";

const url = {
  register: () => `/auth/register`,
  login: () => `/auth/login`,
  allUser: () => "/users",
  detailUser: (param) => `/users/${param}`,
  ownerApproval: () => `/users/role/owner`,
  userActive: () => `/users/status/aktif`,
  updateStatus: (id) => `/users/${id}`,
  rejectStatus: (id) => `/users/reject/${id}`,
  profile: () => `/users/profile`,
  editProfile: () => `/users/profile/edit`,
};

const hooks = {
  getallUser() {
    return useSWR(url.allUser(), http.fetcher);
  },
  getDetailUser(id) {
    return useSWR(url.detailUser(id), http.fetcher);
  },
  getOwner() {
    return useSWR(url.ownerApproval(), http.fetcher);
  },
  getProfile() {
    return useSWR(url.profile(), http.fetcher);
  },
  getActiveUser() {
    return useSWR(url.userActive(), http.fetcher);
  },
};

const manipulateData = {
  postRegister(data) {
    return http.post(url.register()).send(data);
  },
  postLogin(data) {
    return http.post(url.login()).send(data);
  },
  updateStatus(id) {
    return http.put(url.updateStatus(id)).send();
  },
  rejectStatus(id) {
    return http.put(url.rejectStatus(id)).send();
  },
  updateProfile(data) {
    return http.put(url.editProfile()).send(data);
  },
};

const UserRepository = {
  url,
  manipulateData,
  hooks,
};

export default UserRepository;
