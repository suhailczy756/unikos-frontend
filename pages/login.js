import PrimaryButton from "../components/Buttons/PrimaryButton";
import React, { useState } from "react";
import { Button, Form, Input, message, Radio } from "antd";
import Link from "next/link";
import { useRouter } from "next/router";
import UserRepository from "../repository/user";
import Head from "next/head";

const Register = () => {
  const [formLayout, setFormLayout] = useState("horizontal");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const router = useRouter();

  const handleSubmit = async (e) => {
    try {
      const data = {
        email,
        password,
      };

      const res = await UserRepository.manipulateData.postLogin(data);
      if (res.body.statusCode !== 500) {
        console.log(res.body);
        const token = res.body.token;
        localStorage.setItem("token", token);
        router.push("/");
      } else {
        message.error("Email / Password salah");
      }
    } catch (e) {
      throw e;
    }
  };

  const onFormLayoutChange = ({ layout }) => {
    setFormLayout(layout);
  };

  const formItemLayout =
    formLayout === "horizontal"
      ? {
          labelCol: {
            span: 4,
          },
          wrapperCol: {
            span: 14,
          },
        }
      : null;
  return (
    <div className="flex flex-col items-center justify-center min-h-screen bg-navy">
      <Head>
        <title>Login</title>
      </Head>
      <div className="row w-7/12 flex relative">
        <div className="col bg-white w-1/2 p-10 rounded-l-md">
          <a href="/">
            <img className="absolute left-4 top-5" src="logo.svg" width={100}></img>
          </a>
          <div className="flex flex-col mt-10">
            <h2 className="font-semibold text-xl text-center text-title">Selamat Datang!</h2>
            <div className="w-80 px-5 pt-4">
              <Form
                onFinish={handleSubmit}
                {...formItemLayout}
                layout={formLayout}
                // form={form}
                initialValues={{
                  layout: formLayout,
                }}
                onValuesChange={onFormLayoutChange}
              >
                <div className="mb-3">
                  <span className="text-sm font-semibold">Nama</span>
                  <Form.Item
                    className="text-title font-normal"
                    name={"email"}
                    rules={[
                      {
                        required: true,
                        message: "Email harus diisi",
                      },

                      {
                        type: "email",
                        message: "Email tidak valid",
                      },
                    ]}
                  >
                    <Input
                      name=""
                      className="my-2 p-2 text-[0.7rem] border border-navy rounded w-72"
                      value={email}
                      onChange={(e) => {
                        setEmail(e.target.value);
                      }}
                      placeholder="Masukan Email Kamu"
                    />
                  </Form.Item>{" "}
                </div>
                <div className="mb-3">
                  <span className="text-sm font-semibold">Sandi</span>
                  <Form.Item
                    className="text-title font-normal"
                    name="Sandi"
                    rules={[
                      {
                        required: true,
                        message: "Sandi harus diisi",
                      },
                    ]}
                  >
                    <Input
                      type="password"
                      value={password}
                      onChange={(e) => {
                        setPassword(e.target.value);
                      }}
                      name="password"
                      className="my-2 p-2 text-[0.7rem] border border-navy rounded w-72"
                      placeholder="Masukan Password Kamu"
                    />
                  </Form.Item>{" "}
                </div>
                <div className="mb-3">
                  <PrimaryButton name={"Masuk"} />
                  <label className="font-normal text-xs w-72 flex justify-center mt-2">
                    Belum punya akun?
                    <Link href={"/register"}>
                      <span className="text-primary cursor-pointer ml-1"> Daftar</span>
                    </Link>
                  </label>
                </div>
              </Form>
            </div>
          </div>
        </div>
        <div className="col flex flex-col items-center justify-center bg-[#EDF0F0] rounded-r-md w-1/2 p-10">
          <img src="/image/login.svg"></img>
        </div>
      </div>
    </div>
  );
};

export default Register;
