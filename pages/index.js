import Hero from "../components/Hero";
import Master from "../components/Layout/Master";
import Featured from "../components/Featured";

export default function Home() {
  return (
    <div>
      <Hero />
      <Featured />
    </div>
  );
}
Home.getLayout = function Layout(page) {
  return <Master title="Unikos">{page}</Master>;
};
