import Master from "../../components/Layout/Master";
import SecondaryButton from "../../components/Buttons/SecondaryButton";
import FacilityTable from "../../components/Facility/FacilityTable";
import { useEffect, useState } from "react";
import { Alert, Form, Input, Modal, Select } from "antd";
import { kosRepository } from "../../repository/kos";
import { mutate } from "swr";

const { Option } = Select;

const Index = () => {
  const [form] = Form.useForm();
  const [name, setName] = useState();
  const [response, setResponse] = useState();
  const [typeof_facility, setTypeOf_Facility] = useState();

  const [formLayout, setFormLayout] = useState("vertical");

  const handleSelect = (v) => {
    setTypeOf_Facility(v);
  };

  const formItemLayout =
    formLayout === "vertical"
      ? {
          labelCol: {
            span: 4,
          },
          wrapperCol: {
            span: 14,
          },
        }
      : null;
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleFinish = async () => {
    try {
      const data = {
        name,
        typeof_facility,
      };
      const res = await kosRepository.manipulateData.createFacility(data);
      setResponse(res);
      setName();
      setTypeOf_Facility();
      await mutate(kosRepository.url.allFacility());
    } catch (e) {
      console.log(e);
    }
  };

  const handleClose = () => {
    setResponse("");
  };

  const handleOk = () => {
    setIsModalVisible(false);
    handleFinish();
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <div className="w-9/12 mx-auto min-h-screen">
      <div className="flex justify-between my-8">
        <span className="font-semibold text-2xl">List Fasilitas</span>
        <div className="">
          <SecondaryButton onClick={showModal} name={"Tambah Fasilitas"} />
          <Modal title="Tambah Fasilitas" visible={isModalVisible} onCancel={handleCancel} onOk={handleOk}>
            <Form
              {...formItemLayout}
              layout={formLayout}
              form={form}
              initialValues={{
                layout: formLayout,
              }}
              onFinish={handleFinish}
            >
              <Form.Item>
                <span>Nama Fasilitas</span>
                <Input className="" autoFocus value={name} onChange={(e) => setName(e.target.value)}></Input>
              </Form.Item>
              <Form.Item>
                <span>Tipe Fasilitas</span>
                <Select placeholder={"Pilih tipe"} value={typeof_facility} onChange={handleSelect}>
                  <Option value={"fasilitas umum"}>Fasilitas Umum</Option>
                  <Option value={"fasilitas pribadi"}>Fasilitas Pribadi</Option>
                </Select>
              </Form.Item>
            </Form>
          </Modal>
        </div>
      </div>
      {response ? <Alert closable afterClose={handleClose} message="Data Berhasil Ditambahkan" type="success" /> : ""}
      <FacilityTable />
    </div>
  );
};

export default Index;

Index.getLayout = function Layout(page) {
  return <Master>{page}</Master>;
};
