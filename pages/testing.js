import { Form } from "antd";
import { useState } from "react";

const testing = () => {
  const [data, setData] = useState([]);
  const [checkedValues, setCheckedValues] = useState();
  const handleFinish = () => {
    setData();
  };

  console.log(data);

  // const handleChange = (target) => {
  //   if (target.checked == true) {
  //     setData((current) => [...current, target.value]);
  //   } else {
  //     data.filter(function (f) {
  //       return f !== target.value;
  //     });
  //   }
  // };

  const handleChange = ({ target: { checked, value } }) => {
    if (checked) {
      setData((values) => [...values, value]);
    } else {
      setData((data) => data.filter((item) => item != value));
    }
  };

  return (
    <div>
      <label className="w-2/6 mb-2 block font-semibold text-base">Fasilitas Umum</label>
      <Form onFinish={handleFinish}>
        <div className="">
          <input
            className="appearance-none"
            type="checkbox"
            id="bath"
            name="fasilitas"
            onChange={(e) => {
              handleChange(e);
            }}
            value="Kamar Mandi"
          />
          <label className="border-2 cursor-pointer p-1" htmlFor="bath">
            Kamar Mandi
          </label>
          <input
            className="appearance-none"
            type="checkbox"
            id="bed"
            name="fasilitas"
            onChange={(e) => {
              handleChange(e);
            }}
            value="Kasur"
          />
          <label className="border-2 cursor-pointer p-1" htmlFor="bed">
            Kasur
          </label>
          <input
            className="appearance-none"
            type="checkbox"
            id="field"
            name="fasilitas"
            onChange={(e) => {
              handleChange(e);
            }}
            value="Lapangan"
          />
          <label className="border-2 cursor-pointer p-1" htmlFor="field">
            Lapangan
          </label>
        </div>
        <button>Gas</button>
      </Form>
    </div>
  );
};

export default testing;
