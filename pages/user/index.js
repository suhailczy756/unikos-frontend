import { Breadcrumb, Button, Divider, Input, message, Modal } from "antd";
import React, { useState } from "react";
import UserTable from "../../components/User/UserTable";
import Master from "../../components/Layout/Master";
import Image from "next/image";
import {
  CheckOutlined,
  CloseOutlined,
  LoadingOutlined,
} from "@ant-design/icons";
import UserRepository from "../../repository/user";
import { mutate } from "swr";

export default function User() {
  const [modal, setModal] = useState(false);
  const [loadings, setLoadings] = useState([]);

  const { data: allData } = UserRepository.hooks.getOwner();

  const enterLoading = (index) => {
    setLoadings((prevLoadings) => {
      const newLoadings = [...prevLoadings];
      newLoadings[index] = true;
      return newLoadings;
    });
    setTimeout(() => {
      setLoadings((prevLoadings) => {
        const newLoadings = [...prevLoadings];
        newLoadings[index] = false;
        return newLoadings;
      });
    }, 2000);
  };

  const showModal = () => {
    setModal(true);
  };

  const approveUser = async (id) => {
    try {
      await UserRepository.manipulateData.updateStatus(id);
      mutate(UserRepository.url.ownerApproval);
      message.success("User berhasil dikonfirmasi!");
      await mutate(UserRepository.url.userActive);
    } catch (e) {
      console.log(e);
    }
  };
  const rejectUser = async (id) => {
    try {
      await UserRepository.manipulateData.rejectStatus(id);
      mutate(UserRepository.url.ownerApproval);
      message.success("User berhasil Ditolak!");
      await mutate(UserRepository.url.userActive);
    } catch (e) {
      console.log(e);
    }
  };

  const handleClose = () => {
    setModal(false);
  };

  return (
    <div>
      <div className="w-10/12 mx-32 mt-4">
        <Breadcrumb>
          <Breadcrumb.Item>
            <a href="/dashboard">Dashboard</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item>List Pengguna</Breadcrumb.Item>
        </Breadcrumb>
      </div>
      <div className="flex flex-col w-10/12 mx-32">
        <div className="container flex w-full mt-2 mb-2 justify-between">
          <div className="text-basic text-xl font-semibold inline">
            List Pengguna
          </div>
          <div
            className="text-blue-600 cursor-pointer font-bold"
            onClick={showModal}
          >
            <div hidden={allData?.data[1] === 0}>
              ({allData?.data[1]}) pengguna membutuhkan konfirmasi
            </div>
          </div>
          <Modal
            footer={false}
            title={"Konfirmasi Pengguna"}
            onCancel={handleClose}
            visible={modal}
          >
            <table className="table-fixed w-full">
              {allData?.data[0].map((v) => {
                return (
                  <tr className="flex items-center">
                    <td className="w-2/12">
                      {v?.image == "" ? (
                        <div className="w-8 h-8 flex mb-2 items-center justify-center text-white font-bold rounded-full bg-blue-800">
                          {v?.name.charAt(0)}
                        </div>
                      ) : (
                        <div>
                          <img
                            style={{ height: "40px", width: "40px" }}
                            className="rounded-[50%]"
                            src={v?.image}
                          />
                        </div>
                      )}
                    </td>
                    <td className="w-full">
                      {v.name} - {v.phone_numbers}
                    </td>
                    <td className="text-right w-1/2 space-x-2">
                      <Button
                        icon={<CloseOutlined />}
                        onClick={() => rejectUser(v.id)}
                        className="bg-red-500 text-white"
                      ></Button>
                      <Button
                        loading={loadings[1]}
                        icon={<CheckOutlined />}
                        onClick={() => approveUser(v.id)}
                        className="bg-green-500 text-white"
                      ></Button>{" "}
                    </td>
                  </tr>
                );
              })}
            </table>
            <Divider />
            <div className="flex justify-end">
              <button
                onClick={handleClose}
                className="bg-blue-500 text-white rounded-sm py-1 px-3 right-0"
              >
                Tutup
              </button>
            </div>
          </Modal>
          <Input className="w-1/4" placeholder="Cari Nama Pengguna" />
        </div>
        <UserTable />
      </div>
    </div>
  );
}

User.getLayout = function Layout(page) {
  return <Master title={"User | Unikos"}>{page}</Master>;
};
