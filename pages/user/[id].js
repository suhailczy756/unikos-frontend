import { Input, Form, Image, Tag } from "antd";
import React, { useState } from "react";
import { Select } from "antd";
import Link from "next/link";
import Master from "../../components/Layout/Master";
import UserRepository from "../../repository/user";
import { useRouter } from "next/router";

const { Option } = Select;
const handleChange = (value) => {
  console.log(`selected ${value}`);
};

const UserDetail = () => {
  const router = useRouter();
  const { id } = router.query;
  const { data: userData } = UserRepository.hooks.getDetailUser(id);

  const createDate = new Date(userData?.data.created_at).toLocaleDateString("id-ID", { dateStyle: "full" });
  const updateDate = new Date(userData?.data.updated_at).toLocaleDateString("id-ID", { dateStyle: "full" });
  const [form] = Form.useForm();
  const [formLayout, setFormLayout] = useState("vertical");

  const onFormLayoutChange = ({ layout }) => {
    setFormLayout(layout);
  };

  const formItemLayout =
    formLayout === "horizontal"
      ? {
          labelCol: {
            span: 4,
          },
          wrapperCol: {
            span: 0,
          },
        }
      : null;

  return (
    userData && (
      <div>
        <Form
          {...formItemLayout}
          layout={formLayout}
          form={form}
          initialValues={{
            layout: formLayout,
          }}
          onValuesChange={onFormLayoutChange}
        >
          <div className="mx-auto mt-10 w-6/12 bg-white shadow-md">
            <div className="flex flex-col p-5 ">
              {userData?.data?.status == "tertolak" ? (
                <Tag className="w-min" color={"#cd201f"}>
                  Tertolak
                </Tag>
              ) : null}
              {userData?.data?.status == "aktif" ? (
                <Tag className="w-min" color={"green"}>
                  Aktif
                </Tag>
              ) : null}
              {userData?.data?.status == "tertunda" ? (
                <Tag className="w-min" color={"gray"}>
                  Tertunda
                </Tag>
              ) : null}
              <div className="flex justify-center">
                {userData?.data.image == "" ? (
                  <div className="w-20 h-20 flex items-center justify-center text-2xl text-white font-bold rounded-full bg-blue-800">{userData?.data?.name?.charAt(0)}</div>
                ) : (
                  <Image preview={false} src={userData?.data.image} width={150} />
                )}
              </div>
              <div className="w-full flex flex-col p-4">
                <div className="flex flex-row gap-x-6">
                  <Form.Item className="w-full" label={<label style={{ fontWeight: "600" }}>Nama</label>}>
                    <Input disabled className="" value={userData?.data.name} />
                  </Form.Item>
                  <Form.Item className="w-full" label={<label style={{ fontWeight: "600" }}>Email</label>}>
                    <Input disabled className=" " value={userData?.data.email} />
                  </Form.Item>
                </div>
                <div className="flex flex-row gap-x-6">
                  <Form.Item className="w-full" label={<label style={{ fontWeight: "600" }}>No Telp</label>}>
                    <Input disabled className="" value={"+" + userData?.data.phone_numbers} />
                  </Form.Item>
                  <Form.Item className="w-full" label={<label style={{ fontWeight: "600" }}>Level Pengguna</label>}>
                    <Select defaultValue={userData?.data?.role.name} onChange={handleChange}>
                      <Option value="pemilik">Pemiik</Option>
                      <Option value="penyewa">Penyewa</Option>
                      <Option value="admin">Admin</Option>
                    </Select>
                  </Form.Item>
                </div>
                <div className="flex flex-row gap-x-6">
                  <Form.Item className="w-full" label={<label style={{ fontWeight: "600" }}>Tanggal Buat</label>}>
                    <Input disabled className="" value={createDate} />
                  </Form.Item>
                  <Form.Item className="w-full" label={<label style={{ fontWeight: "600" }}>Tanggal Ubah</label>}>
                    <Input disabled className=" " value={updateDate} />
                  </Form.Item>
                </div>
              </div>
              <div className="flex flex-row justify-center gap-7 font-bold">
                <Link href="/user">Kembali</Link>
                <Link href="/save">Simpan</Link>
              </div>
            </div>
          </div>
        </Form>
      </div>
    )
  );
};

export default UserDetail;

UserDetail.getLayout = function Layout(page) {
  return <Master title={`Detail User | Unikos`}>{page}</Master>;
};
