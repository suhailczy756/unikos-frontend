import { Breadcrumb } from "antd";
import { useRouter } from "next/router";
import BottomSection from "../../components/Kos/BottomSection";
import ImageContent from "../../components/Kos/ImageContent";
import MainContent from "../../components/Kos/MainContent";
import Ordered from "../../components/Kos/Ordered";
import Master from "../../components/Layout/Master";
import OrderContent from "../../components/OrderContent";
import { TransactionRepository } from "../../repository/transaction";

const DetailOrder = () => {
  const route = useRouter();
  const { id } = route.query;

  function ordered() {
    return <Ordered />;
  }

  const { data: detailData } =
    TransactionRepository.hooks.getMyTransactionDetail(id);
  const boarding = detailData?.boarding?.id;

  return (
    <div className="w-10/12 mx-auto mt-4">
      <Breadcrumb>
        <Breadcrumb.Item>
          <a href="/">Home</a>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <a href="/order">Pesanan Saya</a>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Detail Pesanan</Breadcrumb.Item>
      </Breadcrumb>
      <div className="flex justify-between mt-4">
        <ImageContent id={boarding} />
        {detailData?.invoice == null ? (
          <OrderContent id={detailData} />
        ) : (
          <MainContent id={boarding} />
        )}
        {ordered()}
      </div>
      <BottomSection id={boarding} />
    </div>
  );
};

export default DetailOrder;

DetailOrder.getLayout = function layout(page) {
  return <Master title={"Detail Order | Unikos"}>{page}</Master>;
};
