import { Breadcrumb } from "antd";
import Link from "next/link";
import { useRouter } from "next/router";
import SecondaryButton from "../../components/Buttons/SecondaryButton";
import Master from "../../components/Layout/Master";
import MyCard from "../../components/MyCard";
import { TransactionRepository } from "../../repository/transaction";

const Index = () => {
  const route = useRouter();
  const { data: allData } = TransactionRepository.hooks.getMyTransaction();
  console.log(allData);
  const handleClick = async (value) => {
    await route.push(`/order/${value}`);
  };
  return (
    <div className="w-10/12 mx-auto mt-4">
      <Breadcrumb>
        <Breadcrumb.Item>
          <a href="/">Home</a>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Pesanan Saya</Breadcrumb.Item>
      </Breadcrumb>
      <span className="font-bold block my-4 text-xl">Kos Pesanan Saya</span>
      {allData?.length == 0 ? (
        <div className="rounded-sm shadow-md mx-auto">
          <div className="w-1/2 mx-auto text-center flex flex-col py-16 space-y-3">
            <div className="font-semibold text-lg">Kamu belum memesan kos</div>
            <div className="font-medium text-base">
              Kos yang pernah kamu pesan akan ditampilkan di halaman ini
            </div>
            <div className="w-1/4 mx-auto">
              <Link href={"/search"}>
                <SecondaryButton name={"Cari kos"} />
              </Link>
            </div>
          </div>
        </div>
      ) : (
        <div className="grid grid-cols-4">
          {allData?.map((v) => {
            const image = v?.boarding?.image[0]?.name;
            return (
              <div>
                <MyCard
                  onClick={() => {
                    handleClick(v.id);
                  }}
                  name={v?.boarding?.name}
                  status={v?.boarding?.status}
                  image={image}
                  lokasi={`${v?.boarding?.village.name}, ${v?.boarding?.district.name}`}
                  type={v?.boarding?.type}
                  rating={v?.boarding?.rate}
                  price={v?.price}
                />
              </div>
            );
          })}
        </div>
      )}
    </div>
  );
};

export default Index;

Index.getLayout = function Layout(page) {
  return <Master title={"Pesanan Saya | Unikos"}>{page}</Master>;
};
