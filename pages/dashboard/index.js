import Master from "../../components/Layout/Master";
import TopContent from "../../components/DashboardAdmin/TopContent";
import KosRequest from "../../components/DashboardAdmin/KosRequest";
import OwnerRequest from "../../components/DashboardAdmin/OwnerRequest";
import authenticationPage from "../../helper/authenticationPage";
const Index = () => {
  authenticationPage();
  return (
    <div className="w-9/12 mx-auto  my-10 p-4">
      <TopContent />
      <KosRequest />
      <OwnerRequest />
    </div>
  );
};

export default Index;

Index.getLayout = function Layout(page) {
  return <Master title={"Dashboard | Unikos"}>{page}</Master>;
};
