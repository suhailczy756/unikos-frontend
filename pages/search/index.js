import MyCard from "../../components/MyCard";
import SelectionSearch from "../../components/SelectionSearch";
import Master from "../../components/Layout/Master";
import { useEffect, useState } from "react";
import { kosRepository } from "../../repository/kos";
import axios from "axios";
import { useRouter } from "next/router";
import { Breadcrumb } from "antd";

const QuickSearch = () => {
  const [defaultKos, setDefaultKos] = useState();
  const route = useRouter();

  const handleClick = (id) => {
    route.push(`/kos/${id}`);
  };

  const { data: allData } = kosRepository.hooks.getAllKos();

  useEffect(() => {
    if (allData !== null && allData !== undefined) {
      setDefaultKos(allData?.data);
    }
  }, [allData]);

  const onSearch = async (page, pageSize, province = undefined, city = undefined, district = undefined, village = undefined) => {
    const res = await axios.get("http://localhost:3222/kos/search/search", {
      params: { page, pageSize, province, city, district, village },
    });
    console.log({ page, pageSize, province, city, district, village });
    setDefaultKos(res.data);
    console.log(res.data);
  };

  return (
    <div className="w-10/12 mx-auto my-4">
      <Breadcrumb>
        <Breadcrumb.Item>
          <a href="/">Home</a>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Pencarian Cepat</Breadcrumb.Item>
      </Breadcrumb>
      <span className="font-bold block my-4 text-xl">Pencarian Cepat</span>
      <SelectionSearch onSearch={onSearch} />
      {defaultKos?.length == 0 ? (
        <div className="flex flex-col items-center py-10 space-y-3">
          <img src="/image/searchkos.svg" width={300}></img>
          <div className="font-medium text-lg">Kosan yang kamu cari tidak ada</div>
        </div>
      ) : (
        <div className="w-full mt-2 grid grid-cols-4 gap-y-6 gap-x-5 justify-between">
          {defaultKos?.map((v) => {
            return (
              <MyCard
                onClick={() => {
                  handleClick(v.id);
                }}
                name={v.name}
                status={v.status}
                image={v.image[0].name}
                city={`${v.city?.name}`}
                type={v.type}
                rating={v.rate}
                price={v.price}
              />
            );
          })}
        </div>
      )}
    </div>
  );
};

export default QuickSearch;

QuickSearch.getLayout = function Layout(page) {
  return <Master title="Cari Kos | Unikos">{page}</Master>;
};
