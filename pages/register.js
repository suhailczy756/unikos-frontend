import React, { useState } from "react";
import RegisterForm from "../components/Auth/RegisterForm";

const Register = () => {
  return (
    <div className="flex flex-col items-center justify-center min-h-screen bg-navy">
      <div className="row w-7/12 flex relative">
        <div className="col flex flex-col items-center justify-center bg-[#EDF0F0] rounded-l-md w-1/2 p-10">
          <img src="/image/register.svg"></img>
        </div>
        <div className="col bg-white w-1/2 p-10 rounded-r-md">
          <img className="absolute right-2 top-3" src="logo.svg" width={100}></img>
          <div className="flex flex-col mt-10">
            <h2 className="font-bold text-xl text-center text-title">Daftar Sekarang</h2>
            <div className="w-80 px-5 pt-4">
              <RegisterForm />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Register;
