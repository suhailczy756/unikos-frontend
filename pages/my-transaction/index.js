import { Breadcrumb } from "antd";
import { useRouter } from "next/router";
import SecondaryButton from "../../components/Buttons/SecondaryButton";
import Master from "../../components/Layout/Master";
import formatCurrency from "../../helper/formatCurrency";
import { TransactionRepository } from "../../repository/transaction";

const Index = () => {
  const { data: allData } = TransactionRepository.hooks.getOwnerTrasaction();
  const route = useRouter();

  const handleClick = async (id) => {
    await route.push(`/my-transaction/${id}`);
  };
  return (
    <div className="w-10/12 mx-auto mt-4">
      <Breadcrumb>
        <Breadcrumb.Item>
          <a href="/">Home</a>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Transaksi Saya</Breadcrumb.Item>
      </Breadcrumb>
      <span className="font-semibold text-xl">Transaksi Saya</span>
      {allData?.length == 0 ? (
        <div className="rounded-sm shadow-md mx-auto">
          <div className="w-1/2 mx-auto text-center flex flex-col py-16 space-y-3">
            <div className="font-semibold text-lg">Kamu belum ada transaksi</div>
            <div className="font-medium text-base">Transaksi kamu akan ditampilkan di halaman ini</div>
          </div>
        </div>
      ) : (
        allData?.data[0].map((v) => {
          return (
            <div className="border shadow-md mt-4 w-full p-5 flex justify-between items-center">
              <div>{v?.boarding?.name}</div>
              <div>{v?.status}</div>
              <div>{formatCurrency(v?.price)}</div>
              <div>
                <SecondaryButton
                  onClick={() => {
                    handleClick(v?.id);
                  }}
                  name={"Detail"}
                />
              </div>
            </div>
          );
        })
      )}
    </div>
  );
};

export default Index;

Index.getLayout = function layout(page) {
  return <Master>{page}</Master>;
};
