import Master from "../../components/Layout/Master";
import Link from "next/link";
import { useRouter } from "next/router";
import { TransactionRepository } from "../../repository/transaction";
import { Breadcrumb, Button, Image, message, Modal } from "antd";
import React, { useState } from "react";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import formatDate from "../../helper/formatDate";
import formatCurrency from "../../helper/formatCurrency";

const { confirm } = Modal;

const Detail = () => {
  const route = useRouter();
  const { id } = route.query;
  const { data: detailData } = TransactionRepository.hooks.getDetail(id);
  const result = detailData?.data;

  const handleConfirm = (id) => {
    confirm({
      title: "Konfirmasi Pembayaran",
      icon: <ExclamationCircleOutlined />,
      content: "Transaksi yang sudah di konfirmasi tidak akan bisa diubah lagi",

      async onOk() {
        await TransactionRepository.manipulateData.approveStatus(id);
        message.success("Transaksi Berhasil DiKonfirmasi");

        return new Promise((resolve, reject) => {
          setTimeout(Math.random() > 0.5 ? resolve : reject, 500);
          route.push("/my-transaction");
        }).catch((error) => console.log(error));
      },

      onCancel() {},
    });
  };
  const handleReject = (id) => {
    confirm({
      title: "Tolak Pembayaran",
      icon: <ExclamationCircleOutlined />,
      content: "Transaksi yang sudah di tolak tidak akan bisa diubah lagi",
      okType: "danger",

      async onOk() {
        await TransactionRepository.manipulateData.rejectStatus(id);
        message.success("Transaksi Berhasil Ditolak");

        return new Promise((resolve, reject) => {
          setTimeout(Math.random() > 0.5 ? resolve : reject, 500);
          route.push("/my-transaction");
        }).catch((error) => console.log(error));
      },

      onCancel() {},
    });
  };

  return (
    <div className="w-9/12 mt-4 mx-auto">
      <Breadcrumb>
        <Breadcrumb.Item>
          <a href="/">Home</a>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <a href="/my-kos">Transaksi Saya</a>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Detail Transaksi</Breadcrumb.Item>
      </Breadcrumb>
      <div className="font-semibold text-xl my-4">Detail Transaksi</div>
      <div className="flex justify-between space-x-4">
        <div className="w-3/4 shadow-lg border p-4 space-y-8 rounded-md">
          <div className="flex">
            <div className="font-semibold w-1/2">Tanggal Pemesanan</div>
            <div className="text-left">{formatDate(result?.created_at)}</div>
          </div>
          <div className="flex">
            <div className="font-semibold w-1/2">Nama Penyewa</div>
            <div className="text-left">{result?.user.name}</div>
          </div>
          <div className="flex">
            <div className="font-semibold w-1/2">Nama Kos</div>
            <div className="text-left">{result?.boarding.name}</div>
          </div>
          <div className="flex">
            <div className="font-semibold w-1/2">Nama Pemilik</div>
            <div className="text-left">{result?.boarding.user.name}</div>
          </div>
        </div>
        <div className="w-3/4 p-4 text-center shadow-lg border space-x-4 rounded-md">
          <div className="font-semibold">Bukti Transaksi</div>
          <Image src={result?.invoice} width={150} height={150} />
        </div>
      </div>

      <div className="w-full mt-4 shadow-md border">
        <div className="p-5 rounded-md">
          <table className="table-fixed w-full">
            <thead className="text-center font-bold">
              <tr>
                <td>Tanggal Masuk</td>
                <td>Tanggal Keluar</td>
                <td>Total</td>
              </tr>
            </thead>
            <tbody className="text-center">
              <tr>
                <td>{formatDate(result?.start_date)}</td>
                <td>{formatDate(result?.end_date)}</td>
                <td>{formatCurrency(result?.price)}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div className="w-full flex justify-between mt-4">
        <div className="font-bold text-lg">
          <Link href={"/my-transaction"}>Kembali</Link>
        </div>
        {result?.status == "tertunda" ? (
          <div className="flex space-x-2 justify-between">
            <div>
              <Button
                onClick={() => {
                  handleReject(result?.id);
                }}
                className="bg-red-500 font-bold text-md text-white hover:bg-red-600"
              >
                Tolak
              </Button>
            </div>

            {/* Approve Modal */}
            <div>
              <Button
                onClick={() => {
                  handleConfirm(result?.id);
                }}
                className="bg-green-500 font-bold text-md text-white hover:bg-green-600"
              >
                Terima
              </Button>
            </div>
          </div>
        ) : null}
      </div>
    </div>
  );
};
export default Detail;

Detail.getLayout = function Layout(page) {
  return <Master>{page}</Master>;
};
