import React, { useEffect, useState } from "react";
import axios from "axios";
import Cards from "../../components/Cards";
import Master from "../../components/Layout/Master";
import Link from "next/link";
import FormEditKos from "../../components/FormEditKos";
import { useRouter } from "next/router";
import { kosRepository } from "../../repository/kos";
import authenticationPage from "../../helper/authenticationPage";
import { mutate } from "swr";

export default function EditKos() {
  authenticationPage();
  const [kosState, setKos] = useState("");
  const route = useRouter();
  const { id } = route.query;
  const { data: dataKos } = kosRepository.hooks.useDetailKost(id);
  console.log(dataKos);
  return (
    <>
      <FormEditKos dataKos={dataKos?.data} />
    </>
  );
}

EditKos.getLayout = function Layout(page) {
  return <Master title={"Ubah Kos | Unikos"}>{page}</Master>;
};
