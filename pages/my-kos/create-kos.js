import { Form, Slider } from "antd";
import React, { useEffect, useState } from "react";
import PrimaryButton from "../../components/Buttons/PrimaryButton";
import FormTambahKos from "../../components/FormTambahKos";
import Master from "../../components/Layout/Master";
import authenticationPage from "../../helper/authenticationPage";
import { parseJwt } from "../../helper/parseJwt";

function CreateKos() {
  authenticationPage();
  const [dataKos, setDataKos] = useState("");

  console.log(dataKos);
  const [formLayout, setFormLayout] = useState("vertical");
  const [index, setIndex] = useState(0);

  const onFormLayoutChange = ({ layout }) => {
    setFormLayout(layout);
  };

  const formItemLayout =
    formLayout === "horizontal"
      ? {
          labelCol: {
            span: 4,
          },
          wrapperCol: {
            span: 14,
          },
        }
      : null;

  const handleChange = (value) => {
    console.log(`selected ${value}`);
  };

  const getStep = (value) => {
    setIndex(index + value);
  };

  const backStep = (value) => {
    setIndex(index - value);
  };

  return (
    <div className="w-3/6 mx-auto mt-10">
      <div className="w-3/4 mx-auto">
        <h1 className="text-center font-bold text-xl">Tambah Kos Baru</h1>
        <Form.Item name="slider">
          {console.log("")}
          <Slider
            trackStyle={{ backgroundColor: "#0A253B" }}
            handleStyle={{ backgroundColor: "#0A253B" }}
            max={60}
            step={20}
            value={index}
            marks={{
              0: "Profil Kos",
              20: "Lokasi",
              40: "Fasilitas",
              60: "Harga",
            }}
          />
        </Form.Item>
      </div>
      <Form
        {...formItemLayout}
        layout={formLayout}
        // form={form}
        initialValues={{
          layout: formLayout,
        }}
        onValuesChange={onFormLayoutChange}
      >
        <FormTambahKos getStep={getStep} backStep={backStep} step={index} />
      </Form>
    </div>
  );
}

CreateKos.getLayout = function Layout(page) {
  return <Master title={"Buat Kos Baru"}>{page}</Master>;
};

export default CreateKos;
