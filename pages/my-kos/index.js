import React from "react";
import Master from "../../components/Layout/Master";
import ContentMyKos from "../../components/ContentMyKos";
import { mutate } from "swr";
import { kosRepository } from "../../repository/kos";

export default function MyKos() {
  return (
    <div>
      <ContentMyKos />
    </div>
  );
}

MyKos.getLayout = function Layout(page) {
  return <Master title={"Kos Saya | Unikos"}>{page}</Master>;
};
