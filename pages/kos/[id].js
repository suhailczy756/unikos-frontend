import BottomSection from "../../components/Kos/BottomSection";
import Cart from "../../components/Kos/Cart";
import Ordered from "../../components/Kos/Ordered";
import ImageContent from "../../components/Kos/ImageContent";
import MainContent from "../../components/Kos/MainContent";
import Master from "../../components/Layout/Master";
import { useRouter } from "next/router";
import { kosRepository } from "../../repository/kos";
import Confirmation from "../../components/Kos/Confirmation";
import UserRepository from "../../repository/user";
import ConfirmedAdmin from "../../components/Kos/ConfirmedAdmin";
import Filled from "../../components/Kos/Filled";
import { Breadcrumb } from "antd";

const Detail = () => {
  const { data: Profile } = UserRepository.hooks.getProfile();
  const route = useRouter();
  const { id } = route.query;

  function unordered() {
    return <Cart id={id} />;
  }

  const { data: detailData } = kosRepository.hooks.useDetailKost(id);
  const status = detailData?.data?.status;
  return (
    <div className="w-10/12 py-4 mx-auto">
      <Breadcrumb>
        <Breadcrumb.Item>
          <a href="/">Home</a>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <a href="/search">Pencarian Cepat</a>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Detail Kos</Breadcrumb.Item>
      </Breadcrumb>
      <div className="flex justify-between mt-4">
        <ImageContent id={id} />
        <MainContent id={id} />
        {status === "tersedia" && Profile?.role == "client"
          ? unordered()
          : null}
        {Profile?.role == "admin" && status == "tertunda" ? (
          <Confirmation id={id} />
        ) : null}
        {Profile?.role == "admin" && status == "tersedia" ? (
          <ConfirmedAdmin />
        ) : null}
        {/* {Profile?.role == "owner" ? <Filled id={id} /> : null} */}
      </div>
      <BottomSection id={id} />
    </div>
  );
};

export default Detail;

Detail.getLayout = function Layout(page) {
  return <Master title={`Detail Kos | Unikos`}>{page}</Master>;
};
