import { useRouter } from "next/router";
import Master from "../../components/Layout/Master";
import MyCard from "../../components/MyCard";
import { kosRepository } from "../../repository/kos";

const Index = () => {
  const { data: allData } = kosRepository.hooks.getAllKos();
  const route = useRouter();

  const handleClick = async (value) => {
    await route.push(`/kos/${value}`);
  };

  return (
    <div className="mx-auto w-10/12 mt-4">
      <h2 className="font-semibold text-2xl">List Kosan</h2>
      <div className="grid grid-cols-3 gap-2">
        {allData?.data?.map((v) => {
          const image = v.image[4].name;

          return (
            <div>
              <MyCard
                onClick={() => {
                  handleClick(v.id);
                }}
                name={v.name}
                status={v.status}
                image={image}
                lokasi={v.address}
                type={v.type}
                rating={v.review}
                price={v.price}
              />
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Index;

Index.getLayout = function Layout(page) {
  return <Master>{page}</Master>;
};
