import Master from "../../components/Layout/Master";
import { Form, Image, Input, message } from "antd";
import React, { useEffect, useState } from "react";
import PrimaryButton from "../../components/Buttons/PrimaryButton";
import UserRepository from "../../repository/user";
import UploadProfile from "../../components/Profile/UploadProfile";
import { mutate } from "swr";

const Index = () => {
  const { data: Profile } = UserRepository.hooks.getProfile();

  const [name, setName] = useState();
  const [email, setEmail] = useState();
  const [phone, setPhone] = useState();
  const [fileList, setFileList] = useState([]);
  const [image, setImage] = useState();

  useEffect(() => {
    if (Profile !== null && Profile !== undefined) {
      setName(Profile?.name);
      setEmail(Profile?.email);
      setPhone(Profile?.phone);
      setImage([
        {
          url: Profile?.image,
          name: "Album",
        },
      ]);
    }
  }, [Profile]);

  const handleFinish = async () => {
    try {
      let imagee = image[0] ? image[0].url : undefined;

      await UserRepository.manipulateData.updateProfile({
        name,
        email,
        phone,
        image: imagee,
      });
      await mutate(UserRepository.url.profile());
      message.success("Profil Berhasil Diubah");
    } catch (error) {
      console.log(error);
    }
  };
  const getImage = async (value) => {
    setImage(value);
  };

  return (
    Profile && (
      <div className="w-10/12 mx-auto py-10">
        <Form onFinish={handleFinish}>
          <div className="flex space-x-2">
            <div className="w-2/6 flex justify-items-center flex-col text-center">
              <div className="w-full flex justify-center">
                {Profile?.image == "" ? (
                  <div className="w-28 h-28 flex items-center justify-center text-3xl text-white font-bold rounded-full bg-blue-800">
                    {Profile?.name.charAt(0)}
                  </div>
                ) : (
                  <Image
                    className="rounded-full"
                    preview={false}
                    src={Profile.image}
                    width={150}
                    height={150}
                  />
                )}
              </div>
              <div className="font-bold text-lg text-center mb-2">
                {Profile?.role}
              </div>
              <div className="flex justify-center">
                <div className="w-1/2">
                  <UploadProfile getImage={getImage} />
                </div>
              </div>
            </div>
            <div className="w-3/6 space-y-1">
              <div className="font-bold text-xl mb-7">Profil Saya</div>
              <div className="font-semibold text-lg">Nama</div>
              <Form.Item name="name">
                <Input
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                  placeholder=""
                  defaultValue={Profile?.name}
                />
              </Form.Item>
              <div className="font-semibold text-lg pt-4">Email</div>
              <Form.Item name="email" id="email">
                <Input
                  value={email}
                  placeholder=""
                  onChange={(e) => {
                    setEmail(e.target.value);
                  }}
                  defaultValue={Profile?.email}
                />
              </Form.Item>
              <div className="font-semibold text-lg pt-4">Nomor Telepon</div>
              <Form.Item name="phone">
                <Input
                  type={"number"}
                  onChange={(e) => {
                    setPhone(e.target.value);
                  }}
                  value={phone}
                  defaultValue={Profile?.phone}
                  placeholder=""
                />
              </Form.Item>
              <div className="flex space-x-5 justify-end items-center pt-5">
                <div>
                  <a
                    onClick={() => {
                      window.history.back(1);
                    }}
                    className="text-blue-600 font-bold"
                  >
                    Kembali
                  </a>
                </div>
                <div className="w-1/4">
                  <PrimaryButton name={"Simpan"} />
                </div>
              </div>
            </div>
          </div>
        </Form>
      </div>
    )
  );
};

export default Index;

Index.getLayout = function Layout(page) {
  return <Master>{page}</Master>;
};
