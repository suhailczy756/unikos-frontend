import Master from "../../components/Layout/Master";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { TransactionRepository } from "../../repository/transaction";
import formatDate from "../../helper/formatDate";
import formatCurrency from "../../helper/formatCurrency";
import { Breadcrumb } from "antd";

const Detail = () => {
  const route = useRouter();
  const { id } = route.query;

  const { data: detailData } = TransactionRepository.hooks.getDetail(id);
  const result = detailData?.data;

  const handleClick = () => {
    window.history.back(1);
  };

  return (
    <div className="w-9/12 mx-auto mt-4">
      <Breadcrumb>
        <Breadcrumb.Item>
          <a href="/dashboard">Dashboard</a>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <a href="/transaction">List Transaksi</a>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Detail Transaksi</Breadcrumb.Item>
      </Breadcrumb>
      <div className="font-semibold text-3xl my-4">Detail Transaksi</div>
      <div className="flex justify-between space-x-4">
        <div className="w-3/4 shadow-lg border p-4 space-y-8 rounded-md">
          <div className="flex">
            <div className="font-semibold w-1/2">Tanggal Pemesanan</div>
            <div className="text-left">{formatDate(result?.created_at)}</div>
          </div>
          <div className="flex">
            <div className="font-semibold w-1/2">Nama Penyewa</div>
            <div className="text-left">{result?.user.name}</div>
          </div>
          <div className="flex">
            <div className="font-semibold w-1/2">Nama Kos</div>
            <div className="text-left">{result?.boarding.name}</div>
          </div>
          <div className="flex">
            <div className="font-semibold w-1/2">Nama Pemilik</div>
            <div className="text-left">{result?.boarding.user.name}</div>
          </div>
        </div>
        <div className="w-3/4 p-4 text-center shadow-lg border space-x-4 rounded-md">
          <div className="font-semibold">Bukti Transaksi</div>
          <Image src="/image/kos1.png" width={270} height={170} />
        </div>
      </div>

      <div className="w-full mt-4 shadow-md border">
        <div className="p-5 rounded-md">
          <table className="table-fixed w-full">
            <thead className="text-center font-bold">
              <tr>
                <td>Tanggal Masuk</td>
                <td>Tanggal Keluar</td>
                <td>Total</td>
              </tr>
            </thead>
            <tbody className="text-center">
              <tr>
                <td>{formatDate(result?.start_date)}</td>
                <td>{formatDate(result?.end_date)}</td>
                <td>{formatCurrency(result?.price)}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <button onClick={handleClick}>
        <div className="font-bold cursor-pointer text-blue-500 text-xl my-8">Kembali</div>
      </button>
    </div>
  );
};
export default Detail;

Detail.getLayout = function Layout(page) {
  return <Master title={"Detail Transaksi | Unikos"}>{page}</Master>;
};
