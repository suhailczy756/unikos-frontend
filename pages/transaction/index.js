import { Breadcrumb } from "antd";
import Master from "../../components/Layout/Master";
import Content from "../../components/Transaction/Content";

const Index = () => {
  return (
    <div className="w-10/12 mx-auto mt-4">
      <Breadcrumb>
        <Breadcrumb.Item>
          <a href="/dashboard">Dashboard</a>
        </Breadcrumb.Item>
        <Breadcrumb.Item>List Transaksi</Breadcrumb.Item>
      </Breadcrumb>
      <Content />
    </div>
  );
};

export default Index;

Index.getLayout = function Layout(page) {
  return <Master title="List Transaksi | Unikos">{page}</Master>;
};
