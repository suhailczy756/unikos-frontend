import { Form } from "antd";
import { useState } from "react";

const testing = () => {
  const [data, setData] = useState([]);
  const [checkedValues, setCheckedValues] = useState();
  const handleFinish = (values) => {
    console.log(values);
  };

  const handleChange = (values) => {
    setData(values);
    console.log(values);
  };

  return (
    <div>
      <label className="w-2/6 mb-2 block font-semibold text-base">Fasilitas Umum</label>
      <form
        method="POST"
        onSubmit={(e) => {
          e.preventDefault();
          e.target.fasilitas.forEach((v) => {
            console.log(v.checkedValues);
          });
        }}
      >
        <div className="">
          <input
            className="appearance-none"
            type="checkbox"
            id="bath"
            name="fasilitas"
            onChange={(e) => {
              handleChange(e.target.value);
            }}
            value="Kamar Mandi"
          />
          <label className="border-2 cursor-pointer p-1" for="bath">
            Kamar Mandi
          </label>
          <input
            className="appearance-none"
            type="checkbox"
            id="bed"
            name="fasilitas"
            onChange={(e) => {
              handleChange(e.target.value);
            }}
            value="Kasur"
          />
          <label className="border-2 cursor-pointer p-1" for="bed">
            Kasur
          </label>
          <input
            className="appearance-none"
            type="checkbox"
            id="field"
            name="fasilitas"
            onChange={(e) => {
              handleChange(e.target.value);
            }}
            value="Lapangan"
          />
          <label className="border-2 cursor-pointer p-1" for="field">
            Lapangan
          </label>
        </div>
        <button type="submit">Gas</button>
      </form>
    </div>
  );
};

export default testing;
