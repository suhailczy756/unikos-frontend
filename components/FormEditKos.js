import { Breadcrumb, Button, Form, Input, message, Select } from "antd";
import axios from "axios";
import { get } from "mobx";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { mutate } from "swr";
import formatCurrency from "../helper/formatCurrency";
import { kosRepository } from "../repository/kos";
import Cards from "./Cards";

import EditFacility from "./EditFacility";
import RadioGroup from "./RadioGroup";
import UploadImage from "./UploadImage";

function FormEditKos(props) {
  const [formLayout, setFormLayout] = useState("vertical");
  const [name, setName] = useState();
  const [type, setType] = useState();
  const [status, setStatus] = useState();
  const [price, setPrice] = useState();
  const [description, setDescription] = useState();
  const [notes, setNotes] = useState();
  const [facilities, setFacilities] = useState([]);
  const route = useRouter();
  const { id } = route.query;

  useEffect(() => {
    setName(props.dataKos?.name);
    setDescription(props.dataKos?.description);
  }, [props]);

  const onFormLayoutChange = ({ layout }) => {
    setFormLayout(layout);
  };

  const getNewFacility = (value) => {
    setFacilities(value);
  };
  const getImage = async (value) => {
    setImage(value);
  };

  const getType = (value) => {
    setType(value);
  };

  const formItemLayout =
    formLayout === "horizontal"
      ? {
          labelCol: {
            span: 4,
          },
          wrapperCol: {
            span: 14,
          },
        }
      : null;

  const { Option } = Select;

  const handleFinish = async () => {
    try {
      const datas = {
        name,
        type,
        status,
        price,
        notes,
        description,
        facilities,
      };
      await kosRepository.manipulateData.updateKos(id, datas);
      await message.success("data berhasil disimpan!", 0.5);
      await location.replace("/my-kos");
    } catch (e) {
      console.log(e);
    }
  };

  const handleProvince = (value) => {
    let str = value.split(",");
    setProvince([str[0], str[1]]);
    getCity(str[0]);
  };

  const handleCity = async (value) => {
    let str = value.split(",");
    setCity(str);
    getDistrict(str[0]);
  };

  const handleDistrict = async (value) => {
    let str = value.split(",");
    setDistrict(str);
    getVillage(str[0]);
  };

  const handleVillage = async (value) => {
    let str = value.split(",");
    setVillage(str);
  };

  const { data: ListProvince } = kosRepository.hooks.getProvince();

  const getCity = async (id) => {
    const res = await axios.get(`http://localhost:3222/location/city/${id}`);
    setlistCity(res.data);
  };

  const handleStatus = (value) => {
    setStatus(value);
  };

  const getDistrict = async (id) => {
    const res = await axios.get(
      `http://localhost:3222/location/district/${id}`
    );
    setlistDistrict(res.data);
  };

  const getVillage = async (id) => {
    const res = await axios.get(`http://localhost:3222/location/village/${id}`);
    setlistVillage(res.data);
  };

  return (
    props.dataKos?.name && (
      <>
        <div className="w-10/12 mx-auto flex justify-between mt-4">
          <div className="w-11/12">
            <Form
              onFinish={handleFinish}
              {...formItemLayout}
              layout={formLayout}
              // form={form}
              initialValues={{
                layout: formLayout,
              }}
              onValuesChange={onFormLayoutChange}
            >
              <Breadcrumb>
                <Breadcrumb.Item>
                  <a href="/">Home</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                  <a href="/my-kos">Kosan Saya</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>{props.dataKos.name}</Breadcrumb.Item>
              </Breadcrumb>
              <div className="flex justify-between mt-2">
                <h1 className="text-xl font-semibold">Kelola Kos</h1>
                <Select
                  className="flex items-center"
                  defaultValue={props.dataKos.status}
                  style={{ width: 120 }}
                  onChange={handleStatus}
                >
                  <Option value="terisi">terisi</Option>
                  <Option value="tersedia">tersedia</Option>
                </Select>
              </div>

              <label className="font-semibold block mt-2 text-base">
                Nama Kos
              </label>
              <Form.Item
                name="name"
                rules={[
                  {
                    required: name ? false : true,
                    message: "Harap masukkan nama kos!",
                  },
                  {
                    max: 25,
                    message: "Nama kos terlalu panjang!",
                  },
                ]}
              >
                <Input
                  placeholder="Masukan nama kos anda"
                  value={name}
                  defaultValue={props.dataKos.name}
                  maxLength={26}
                  onChange={(e) => setName(e.target.value)}
                  className=" w-full mt-2 border border-navy"
                />
              </Form.Item>
              <label className="font-semibold text-base">Deskripsi</label>
              <Form.Item
                name="description"
                rules={[
                  {
                    required: description ? false : true,
                    message: "Harap masukkan deskripsi kosan!",
                  },
                ]}
              >
                <Input
                  className="mt-2 border border-navy"
                  onChange={(e) => {
                    setDescription(e.target.value);
                  }}
                  value={description}
                  defaultValue={props.dataKos.description}
                ></Input>
              </Form.Item>

              <div className="">
                <label className="font-semibold text-base block mb-2">
                  Fasilitas
                </label>
                <EditFacility
                  facilities={props.dataKos.facilities}
                  getNewFacility={getNewFacility}
                />
              </div>
              <div className="mt-5">
                <label className="font-semibold text-base block mb-2">
                  Tipe Kosan
                </label>
                <RadioGroup type={props.dataKos.type} getType={getType} />
              </div>
              <div className="mt-5">
                <label className="font-semibold text-base block mb-2">
                  Biaya Kos
                </label>
                <Input
                  type={"number"}
                  className="w-52 border-navy border"
                  addonBefore="Rp"
                  defaultValue={props.dataKos.price}
                  onChange={(e) => {
                    setPrice(e.target.value);
                  }}
                />
              </div>
              <div className="my-5">
                <label className="font-semibold text-base">Catatan</label>
                <Input
                  className="mt-2 border border-navy"
                  name="nama"
                  onChange={(e) => {
                    setNotes(e.target.value);
                  }}
                  defaultValue={props.dataKos.notes}
                ></Input>
              </div>

              <div className="mb-10 flex justify-end font-bold">
                <button className="font-bold text-blue-500" type="submit">
                  Simpan
                </button>
                <a href=""></a>
              </div>
            </Form>
          </div>
          <Cards
            name={name ? name : props.dataKos.name}
            description={description ? description : props.dataKos.description}
            facilities={facilities ? facilities : null}
            type={type ? type : props.dataKos.type}
            status={status ? status : props.dataKos.status}
            createdAt={props.dataKos.createdAt}
            updatedAt={props.dataKos.updatedAt}
          />
        </div>
      </>
    )
  );
}

export default FormEditKos;
