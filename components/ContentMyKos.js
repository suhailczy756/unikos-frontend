import React, { Component } from "react";
import TambahButton from "./Buttons/tambahButton";
import ItemsContent from "./ItemsContent";
import PendingOwner from "./My-Kos/PendingOwner";
import UserRepository from "../repository/user";

const ContentMyKos = () => {
  const buttonName = "Tambah Kosan";
  const { data: Profile } = UserRepository.hooks.getProfile();

  return (
    <div className="">
      {Profile?.status == "aktif" ? (
        <div>
          <TambahButton /> <ItemsContent />
        </div>
      ) : (
        <PendingOwner />
      )}
    </div>
  );
};

export default ContentMyKos;
