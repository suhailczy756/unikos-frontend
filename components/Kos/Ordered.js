import { useRouter } from "next/router";
import { TransactionRepository } from "../../repository/transaction";
import Confirmed from "./Confirmed";
import UploadContent from "./UploadContent";

const Ordered = (props) => {
  const route = useRouter();
  const { id } = route.query;
  const { data: detailData } = TransactionRepository.hooks.getMyTransactionDetail(id);
  return <div className="border rounded-lg w-4/6 ml-4 shadow-md p-4">{detailData?.status == "tertunda" ? <UploadContent id={props.id} /> : <Confirmed id={props.id} />}</div>;
};

export default Ordered;
