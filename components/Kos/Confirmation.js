import { useState } from "react";
import { kosRepository } from "../../repository/kos";
import DangerButton from "../Buttons/DangerButton";
import PrimaryButton from "../Buttons/PrimaryButton";
import { message } from "antd";
import { mutate } from "swr";

const Confirmation = (props) => {
  const confirmClick = async (e) => {
    const id = e.target.value;

    await kosRepository.manipulateData.updateStatus({
      approval: "approve",
      id,
    });
    message
      .loading("Harap Tunggu..", 0.5)
      .then(() => message.success(`Kos Berhasil DiKonfirmasi`, 2.5))
      .then(() => mutate(kosRepository.url.detailKost(id)));
  };
  const rejectClick = async (e) => {
    const id = e.target.value;

    await kosRepository.manipulateData.updateStatus({
      approval: "reject",
      id,
    });
    message
      .loading("Harap Tunggu..", 0.5)
      .then(() => message.success(`Kos Berhasil Ditolak`, 2.5))
      .then(() => mutate(kosRepository.url.detailKost(id)));
  };
  return (
    <div className="w-2/4 flex justify-center items-center">
      <div className="w-full space-y-2">
        <PrimaryButton
          value={props.id}
          onClick={(e) => {
            confirmClick(e);
          }}
          name={"Konfirmasi"}
        />
        <DangerButton
          value={props.id}
          onClick={(e) => {
            rejectClick(e);
          }}
          name={"Tolak"}
        />
      </div>
    </div>
  );
};

export default Confirmation;
