import { ManOutlined } from "@ant-design/icons";
import formatCurrency from "../../helper/formatCurrency";
import { kosRepository } from "../../repository/kos";

const MainContent = (props) => {
  const { data: detailData } = kosRepository.hooks.useDetailKost(props.id);
  const city = detailData?.data?.city?.name.toLowerCase();
  const district = detailData?.data?.district?.name.toLowerCase();

  return (
    <div className="w-full pr-10 flex flex-col pl-5">
      <div className="w-full">
        <div className="font-extrabold text-xl">{detailData?.data?.name}</div>
        <div className="font-semibold capitalize text-lg mt-2">
          Pemilik : {detailData?.data?.user?.name}
        </div>
        <div className="font-normal text-base mt-2 capitalize">
          {detailData?.data?.address} {city}, {district}
        </div>
        <div className="font-normal capitalize text-base flex items-center mt-2">
          <ManOutlined className="mr-2" /> Kos {detailData?.data?.type}
        </div>
        <div className="font-bold text-lg mt-2">
          {formatCurrency(detailData?.data?.price)}
        </div>
        <div className="flex flex-col mt-3 w-full">
          <span className="text-lg font-semibold">Fasilitas Umum</span>
          <div className="w-full pt-1 text-sm grid grid-cols-4 px-2 gap-2">
            {detailData?.data?.facilities.map((v) => {
              return v.typeof_facility === "fasilitas umum" ? (
                <span className="text-center text-xs px-[3px] border flex items-center justify-center">
                  {v.name}
                </span>
              ) : null;
            })}
          </div>
        </div>
        <div className="flex flex-col mt-3 w-full">
          <span className="text-lg font-semibold">Fasilitas Pribadi</span>
          <div className="w-full pt-1 text-sm grid grid-cols-4 px-2 gap-2">
            {detailData?.data?.facilities.map((v) => {
              return v.typeof_facility === "fasilitas pribadi" ? (
                <span className="text-center text-xs px-[3px] border flex items-center justify-center">
                  {v.name}
                </span>
              ) : null;
            })}
          </div>
        </div>
        <div className="w-full flex flex-col mt-3">
          <span className="text-lg font-semibold">Catatan</span>
          <div className="border rounded p-2 text-xs">
            {detailData?.data?.notes}
          </div>
        </div>
      </div>
    </div>
  );
};

export default MainContent;
