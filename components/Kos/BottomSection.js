import { useEffect, useState } from "react";
import { kosRepository } from "../../repository/kos";
import { Rate, Image } from "antd";

const BottomSection = (props) => {
  const [data, setData] = useState({});
  const [loading, setLoading] = useState(true);

  const { data: detailData } = kosRepository.hooks.useDetailKost(props.id);

  useEffect(() => {
    setLoading(true);
    if (detailData) {
      setData(detailData.data);
      setLoading(false);
    }
  }, [detailData]);

  return loading ? (
    "...loading"
  ) : (
    <div className="w-8/12 flex flex-col space-y-2">
      <div className="font-bold text-lg">Deskripsi Kos</div>
      <div className="border p-2">{data.description}</div>
      <div className="font-bold text-lg">Ulasan</div>
      <div className="flex flex-col">
        {data.review.length == 0 ? (
          <div className="border text-center text-xs text-subTitle py-4">Belum ada ulasan untuk kosan ini</div>
        ) : (
          data.review.map((v) => {
            return (
              <div className="flex space-x-3 mb-2">
                {v?.user?.image == "" ? (
                  <div className="w-9 h-9 flex items-center justify-center my-auto text-white font-bold rounded-full bg-blue-800">{v?.user?.name.charAt(0)}</div>
                ) : (
                  <Image className="rounded-full" preview={false} src={v.user.image} width={50} />
                )}

                <div className="flex flex-col">
                  <div className="font-semibold">{v.user.name}</div>
                  <Rate className="text-sm" disabled defaultValue={v.rating} />
                  <div className="font-normal">{v.comment}</div>
                </div>
              </div>
            );
          })
        )}
      </div>
    </div>
  );
};

export default BottomSection;
