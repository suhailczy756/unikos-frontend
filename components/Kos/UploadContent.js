import { PlusOutlined } from "@ant-design/icons";
import { Form, message, Modal, Popconfirm, Upload } from "antd";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { imageRepository } from "../../repository/image";
import { TransactionRepository } from "../../repository/transaction";
import ConfirmModal from "../Buttons/ConfirmModal";
import DangerButton from "../Buttons/DangerButton";
import PrimaryButton from "../Buttons/PrimaryButton";
import WhatsappButton from "../Buttons/WhatsappButton";
import UploadTransaction from "../Transaction/UploadTransaction";

const getBase64 = (file) =>
  new Promise((resolve) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onload = () => resolve(reader.result);
  });

const UploadContent = (props) => {
  const route = useRouter();
  const [previewVisible, setPreviewVisible] = useState(false);
  const [previewImage, setPreviewImage] = useState("");
  const [previewTitle, setPreviewTitle] = useState("");
  const [image, setimage] = useState();
  const [fileList, setFileList] = useState([]);
  const { id } = route.query;

  const { data: detailData } = TransactionRepository.hooks.getMyTransactionDetail(id);

  const handleCancel = () => setPreviewVisible(false);

  const handlePreview = async (file) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }

    setPreviewImage(file.url || file.preview);
    setPreviewVisible(true);
    setPreviewTitle(file.name || file.url.substring(file.url.lastIndexOf("/") + 1));
  };

  const handleChange = ({ fileList: newFileList }) => {
    setFileList(newFileList);
  };

  const [visible, setVisible] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [modalText, setModalText] = useState("Proses pembayaran akan diproses 1x24 jam. Jika lebih dari jangka waktu tersebut anda berhak membatalkan pesanan");

  const showModal = () => {
    setVisible(true);
  };

  function refreshPage() {
    window.location.reload(false);
  }

  const handleOk = async () => {
    setConfirmLoading(true);
    setTimeout(() => {
      setVisible(false);
      setConfirmLoading(false);
    }, 2000);
    try {
      const data = { invoice: image[0].url };
      await TransactionRepository.manipulateData.createPayment(detailData?.id, data);
      message.success("Pembayaran Berhasil");
      refreshPage();
    } catch (error) {
      console.log(error);
    }
  };

  const getImage = async (value) => {
    setimage(value);
  };

  const cancelModal = () => {
    setVisible(false);
  };

  const confirm = async (id) => {
    try {
      await TransactionRepository.manipulateData.cancelTransaction(id);
      message
        .loading("Harap Tunggu..", 0.5)
        .then(() => message.success(`Pesanan berhasil dibatalkan`, 2.5))
        .then(() => route.push("/kos"));
    } catch (e) {
      console.log(e);
    }
  };

  const cancel = (e) => {
    console.log(e);
    message.error("Click on No");
  };

  const invoice = detailData?.invoice;

  return (
    <div className="">
      <div className="text-center font-semibold text-lg mb-2">Kosan Ini Sudah Kamu Pesan</div>
      <div className="text-center mb-2">Upload Bukti Transaksi Kamu Untuk Proses Lebih Lanjut</div>
      <div className="grid place-content-center mb-2">{invoice == null ? <UploadTransaction getImage={getImage} /> : <img src={invoice} width={"100"}></img>}</div>

      <div className="text-center mb-2">
        {detailData?.invoice == null ? <PrimaryButton name={"Konfirmasi"} onClick={showModal} /> : null}
        <Modal title="Konfirmasi Pembayaran" visible={visible} onOk={handleOk} confirmLoading={confirmLoading} onCancel={cancelModal}>
          <p>{modalText}</p>
        </Modal>
      </div>
      <div className="mb-2">
        <Popconfirm
          title="Apakah kamu yakin ingin membatalkan pemesanan?"
          placement="bottom"
          onConfirm={() => {
            confirm(id);
          }}
          onCancel={cancel}
          okText="Yes"
          cancelText="No"
        >
          <DangerButton name={"Batalkan Pesanan"} />
        </Popconfirm>
      </div>
      <WhatsappButton />
      <button></button>
    </div>
  );
};

export default UploadContent;
