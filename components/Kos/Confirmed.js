import { useRouter } from "next/router";
import React from "react";
import { TransactionRepository } from "../../repository/transaction";
import ReviewModal from "../Buttons/ReviewModal";
import moment from "moment";
import { Button } from "antd";
import SecondaryButton from "../Buttons/SecondaryButton";
import { PrinterOutlined } from "@ant-design/icons";
import Link from "next/link";

const options = {
  weekday: "long",
  year: "numeric",
  month: "long",
  day: "numeric",
};
moment.locale("id");

const Confirmed = () => {
  const route = useRouter();
  const { id } = route.query;
  const { data: detailData } =
    TransactionRepository.hooks.getMyTransactionDetail(id);
  const invoice = detailData?.invoice;
  const updated = detailData?.updated_at;
  const boardingId = detailData?.boarding?.id;

  return (
    <div>
      <div className="text-center font-semibold text-lg mb-2">
        Pembayaran Kamu Sudah DiKonfirmasi
      </div>
      <div className="grid place-content-center mb-2">
        {<img src={invoice} width={"100"}></img>}
      </div>
      <div className="text-center text-sm mt-4">
        Dikonfirmasi Tanggal :{" "}
        <span className="font-semibold">
          {new Date(updated).toLocaleDateString("id-ID", { dateStyle: "full" })}
        </span>
      </div>
      <div className="flex justify-center">
        <div className="w-9/12 mt-2">
          <ReviewModal boardingId={boardingId} />
        </div>
      </div>
      <div className="flex justify-center">
        <div className="w-9/12 mt-2">
          <Link href={`http://localhost:3222/pdf/${id}`}>
            <span className="rounded w-full bg-primary p-2 font-bold text-white hover:bg-slate-400 flex items-center justify-center cursor-pointer">
              <PrinterOutlined className="mr-2 text-base" />
              Cetak Invoice
            </span>
          </Link>
        </div>
      </div>
    </div>
  );
};
export default Confirmed;
