import { kosRepository } from "../../repository/kos";
import React, { useRef, useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { FreeMode, Navigation, Thumbs } from "swiper";
import "swiper/css";
import "swiper/css/free-mode";
import "swiper/css/navigation";
import "swiper/css/thumbs";

const ImageContent = (props) => {
  const { data: detailData } = kosRepository.hooks.useDetailKost(props.id);
  const [thumbsSwiper, setThumbsSwiper] = useState(null);

  return (
    <div className="w-4/12 space-y-2 mr-5 h-min">
      <Swiper
        style={{
          "--swiper-navigation-color": "#d9d9d9",
          "--swiper-pagination-color": "#d9d9d9",
        }}
        spaceBetween={4}
        navigation={true}
        thumbs={{ swiper: thumbsSwiper && !thumbsSwiper.destroyed ? thumbsSwiper : null }}
        modules={[FreeMode, Navigation, Thumbs]}
        className="mySwiper2"
      >
        {detailData?.data?.image.map((v) => {
          return (
            <SwiperSlide>
              <img src={v.name} />
            </SwiperSlide>
          );
        })}
      </Swiper>
      <Swiper onSwiper={setThumbsSwiper} spaceBetween={10} slidesPerView={4} freeMode={true} watchSlidesProgress={true} modules={[FreeMode, Navigation, Thumbs]} className="mySwiper2">
        {detailData?.data?.image.map((v) => {
          return (
            <SwiperSlide>
              <img src={v.name} />
            </SwiperSlide>
          );
        })}
      </Swiper>
    </div>
  );
};

export default ImageContent;
