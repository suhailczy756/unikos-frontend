import { Select, Space, DatePicker, Form, Button, message } from "antd";
import { useRouter } from "next/router";
import React, { useState } from "react";
import formatCurrency from "../../helper/formatCurrency";
import { kosRepository } from "../../repository/kos";
import { TransactionRepository } from "../../repository/transaction";
import PrimaryButton from "../Buttons/PrimaryButton";
import WhatsappButton from "../Buttons/WhatsappButton";
const { Option } = Select;

const Cart = (props) => {
  const [hari, setHari] = useState("");
  const [end_date, setEnd_Date] = useState("Tanggal Keluar");
  const [start_date, setStart_Date] = useState();
  const [loadings, setLoadings] = useState([]);

  const route = useRouter();
  const { data: detailData } = kosRepository.hooks.useDetailKost(props.id);
  console.log(detailData);
  let price = detailData?.data?.price;
  let totalPrice = price / 30;
  let subPrice = totalPrice * hari;
  let tax = subPrice / 10;
  let dp = subPrice / 5;
  const total = subPrice + tax;

  const onChange = (date, dateString) => {
    try {
      setStart_Date(dateString);
      const end_date = new Date(date);
      end_date.setDate(end_date.getDate() + hari);
      setEnd_Date(new Date(end_date).toLocaleDateString());
    } catch (e) {
      console.log(e);
    }
  };

  const handleFinish = async () => {
    try {
      const data = { price: total, start_date, end_date };
      if (data.price == 0) {
        message.error("Harap masukan lama kamu menginap");
      } else if (data.start_date == undefined) {
        message.error("Harap masukan tanggal mulai");
      }
      const success = await TransactionRepository.manipulateData.createTransaction(props.id, data);

      const idTransaction = success?.body?.data?.id;
      await message.success("Kosan Berhasil Dipesan");
      await route.push(`/order/${idTransaction}`);
      // console.log(data);
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <div className="border rounded-lg w-4/6 shadow-md p-4">
      <div className="text-center font-semibold text-xl mb-2">Pesan Sekarang</div>
      <Form onFinish={handleFinish}>
        <Select aria-required={"true"} className="w-full" placeholder="Lama Menginap" optionFilterProp="children" onChange={setHari}>
          <Option value={30}>30 Hari</Option>
          <Option value={60}>60 Hari</Option>
          <Option value={90}>90 Hari</Option>
        </Select>
        <div className="flex justify-between mt-4">
          <Space direction="vertical">
            <DatePicker format={"MM/DD/YYYY"} placeholder="Tanggal Masuk" onChange={onChange} />
          </Space>
          <Space direction="vertical">
            <DatePicker disabled placeholder={end_date == 0 ? "Tanggal Keluar" : end_date} onChange={onChange} />
          </Space>
        </div>
        <div className="flex justify-between mt-4">
          <div>
            {formatCurrency(price)} x {hari}
          </div>
          <div>{subPrice === 0 ? formatCurrency(price) : formatCurrency(subPrice)}</div>
        </div>
        <div className="flex justify-between mt-4">
          <div>Pajak (10%)</div>
          <div>{tax === 0 ? formatCurrency(price / 10) : formatCurrency(tax)}</div>
        </div>
        <div className="flex justify-between mt-4">
          <div>Dana Pertama Minimal</div>
          <div>{dp === 0 ? formatCurrency(price / 5) : formatCurrency(dp)}</div>
        </div>
        <div className="flex justify-between mt-4 font-semibold mb-4">
          <div>Total</div>
          <div>{formatCurrency(total)}</div>
        </div>
        <PrimaryButton className="rounded w-full text-center mb-3 bg-primary font-bold text-white hover:bg-slate-500" loading={loadings[0]} type="text" name="Pesan">
          Pesan
        </PrimaryButton>
      </Form>
      <br></br>
      <WhatsappButton
        link={`https://api.whatsapp.com/send/?phone=${detailData?.data?.user?.phone_numbers}&text&type=phone_number&app_absent=0
`}
      />
    </div>
  );
};
export default Cart;
