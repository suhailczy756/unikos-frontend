import Link from "next/link";
import PrimaryButton from "../Buttons/PrimaryButton";

const ConfirmedAdmin = () => {
  return (
    <div className="border rounded-lg w-4/6 shadow-md p-4 my-auto">
      <div className="text-center font-semibold text-lg mb-2">Kosan ini sudah dikonfirmasi</div>
      <div className="font-bold text-base text-center">
        <Link href={"/dashboard"}>Kembali ke dashboard</Link>
      </div>
    </div>
  );
};

export default ConfirmedAdmin;
