import { Card, Image, Tag, Tooltip } from "antd";
import { useRouter } from "next/router";
const { Meta } = Card;
import React, { useState } from "react";

import { kosRepository } from "../repository/kos";

const ItemsContent = () => {
  const route = useRouter();
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const router = useRouter();

  const { data: dataKos } = kosRepository.hooks.useKost({
    page,
    pageSize,
  });

  const onSubmitKos = async (e) => {
    try {
      const id = e.target.value;
      router.push(`/my-kos/${id}`);
    } catch (error) {
      console.log(error, " error create kos");
      alert("Failed Create Kost");
    }
  };

  const handleClick = async (id) => {
    route.push(`/kos/${id}`);
  };

  return (
    <div className="w-5/6 mx-auto py-5">
      {dataKos?.data?.length == 0 ? (
        <div className="flex flex-col items-center pt-20 space-y-2">
          <img src="/image/nokos.svg" width={300}></img>
          <div className="font-medium text-base">Kamu belum punya kosan</div>
        </div>
      ) : (
        <div className="grid gap-3 grid-cols-4 grid-rows-2 md:grid-cols-2 lg:grid-cols-4 ">
          {dataKos?.data?.map((v) => {
            return (
              <Card
                bodyStyle={{ padding: "15px", border: 0 }}
                className="rounded drop-shadow-lg"
                cover={
                  <Image
                    onClick={() => {
                      handleClick(v.id);
                    }}
                    alt="example"
                    height={200}
                    preview={false}
                    className={"cursor-pointer hover:shadow-lg"}
                    src={v.image?.name ? v.image?.name : "https://picsum.photos/300/200"}
                  />
                }
              >
                <h3 className="text-base">{v.name}</h3>
                <div className="flex flex-row justify-between mt-2 items-center">
                  {v.status === "tertunda" ? (
                    <Tooltip placement="bottom" title={"Tunggu Konfirmasi"}>
                      <button
                        value={v.id}
                        style={{ padding: "0px 20px 0px 20px" }}
                        disabled
                        className="transition-all ease-in-out rounded border-[1px] border-navy hover:bg-navy hover:text-white"
                        onClick={(e) => {
                          onSubmitKos(e);
                        }}
                      >
                        Kelola
                      </button>
                    </Tooltip>
                  ) : (
                    <button
                      value={v.id}
                      style={{ padding: "0px 20px 0px 20px" }}
                      className="transition-all ease-in-out rounded border-[1px] border-navy hover:bg-navy hover:text-white"
                      onClick={(e) => {
                        onSubmitKos(e);
                      }}
                    >
                      Kelola
                    </button>
                  )}

                  {v.status === "tersedia" ? (
                    <Tag className="cursor-default" style={{ marginRight: "0px", fontSize: "0.7rem" }} color="#87d068">
                      {v.status}
                    </Tag>
                  ) : null}
                  {v.status === "tertunda" ? (
                    <Tag className="cursor-default" style={{ marginRight: "0px", fontSize: "0.7rem" }} color="gray">
                      {v.status}
                    </Tag>
                  ) : null}
                  {v.status === "terisi" ? (
                    <Tag className="cursor-default" style={{ marginRight: "0px", fontSize: "0.7rem" }} color="#1FC1C3">
                      {v.status}
                    </Tag>
                  ) : null}
                  {v.status === "terpesan" ? (
                    <Tag className="cursor-default" style={{ marginRight: "0px", fontSize: "0.7rem" }} color="#108ee9">
                      {v.status}
                    </Tag>
                  ) : null}
                </div>
              </Card>
            );
          })}
        </div>
      )}
    </div>
  );
};

export default ItemsContent;
