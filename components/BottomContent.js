import { Divider, Image, Select } from "antd";
import React from "react";
import PrimaryButton from "./Buttons/PrimaryButton";
const { Option } = Select;

const BottomContent = () => {
  return (
    <div className="bg-navy py-10 flex justify-around">
      <div className="flex">
        <div className="">
          <Image style={{ border: "2px white solid" }} preview={false} className="rounded-full" src="/image/sulthan.png" width={100}></Image>
        </div>
        <div className="ml-4 flex text-white flex-col my-auto ">
          <div className="font-bold text-xl">Sulthan Dzawwadi</div>
          <div className="font-semibold text-lg">mromsulthan@gmail.com</div>
        </div>
      </div>
      <div className="">
        <div className="flex">
          <div className="">
            <Image style={{ border: "2px white solid" }} preview={false} className="rounded-full" src="/image/fahmi.png" width={100}></Image>
          </div>
          <div className="ml-4 flex text-white flex-col my-auto ">
            <div className="font-bold text-xl">Fahmi Dahlan</div>
            <div className="font-semibold text-lg">suhailczy07@gmail.com</div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default BottomContent;
