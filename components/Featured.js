import MySlider from "./MySlider";
import Banner from "./Banner";
import Content from "./Content";
import BottomContent from "./BottomContent.js";
import { useState, useEffect } from "react";
import { kosRepository } from "../repository/kos";
import axios from "axios";
import { mutate } from "swr";

const Featured = () => {
  const [page, setPage] = useState(0);
  const [pageSize, setPageSize] = useState(10);
  const [kos, setKos] = useState();
  const [city, setCity] = useState("KOTA BEKASI");
  const [loading, setLoading] = useState(true);
  const { data: highData } = kosRepository.hooks.getKosTersedia({
    page,
    pageSize,
  });
  const { data: allData } = kosRepository.hooks.getKosByCity({ city });

  const [data, setData] = useState(null);

  useEffect(() => {
    if (allData !== null && allData !== undefined) {
      setKos(allData);
    }
    if (highData) {
      setData(highData);
    }
    setLoading(false);
  }, [allData, highData]);

  const onChange = async (value) => {
    setCity(value);
    setLoading(false);
    if (loading) {
      const res = await axios.get("http://localhost:3222/kos/city/city", {
        params: { city },
      });
      mutate(kosRepository.url.kosByCity(city));
      setKos(res.data);
    }
  };

  return (
    <div className="">
      {loading ? (
        "...loading"
      ) : (
        <>
          <MySlider select={false} highData={data} h2={"Kos Terbaik"} />
          <MySlider onChange={onChange} select={true} kos={kos} h2={"Kos Sekitar"} />
        </>
      )}
      <Banner />
      <Content />
      <BottomContent />
    </div>
  );
};

export default Featured;
