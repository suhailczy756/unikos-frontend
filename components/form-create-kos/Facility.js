import React, { useState } from "react";

const Facilities = (props) => {
  const [privateFacility, setPrivateFacility] = useState([]);
  const [publicFacility, setPublicFacility] = useState([]);

  console.log(publicFacility);

  const handleChangePrivate = ({ target: { checked, value } }) => {
    if (checked) {
      setPrivateFacility((values) => [...values, value]);
      props.getPrivateFacility(privateFacility);
    } else {
      setPrivateFacility((data) => data.filter((item) => item != value));
      props.getPrivateFacility(privateFacility);
    }
  };

  const handleChangePublic = ({ target: { checked, value } }) => {
    if (checked) {
      setPublicFacility((values) => [...values, value]);
      props.getPublicFacility(publicFacility);
    } else {
      setPublicFacility((data) => data.filter((item) => item != value));
      props.getPublicFacility(publicFacility);
    }
  };
  const onChanges = (checkedValues) => {
    props.getPrivateFacility(checkedValues);
  };
  const plainOptions = props.publicFacility.map((v) => {
    return v.name;
  });
  const options = props.privateFacility.map((v) => {
    return v.name;
  });
  return (
    <div>
      <div className="mt-5 items-center">
        <label className="w-2/6 mb-2 block font-semibold text-base">Fasilitas Umum</label>
        <div className="grid grid-cols-4 text-center gap-x-2">
          {props.publicFacility.map((v) => {
            return (
              <div className="w-full">
                <input
                  className="appearance-none"
                  type="checkbox"
                  id={v.id}
                  name="publicFacility"
                  onChange={(e) => {
                    handleChangePublic(e);
                  }}
                  value={v.name}
                />
                <label style={{ marginBottom: -14 }} className="border-2 cursor-pointer p-0 w-full block" htmlFor={v.id}>
                  {v.name}
                </label>
              </div>
            );
          })}
        </div>
      </div>
      <div className="mt-10 items-center">
        <label className="w-2/6 block mb-2 font-semibold text-base">Fasilitas Pribadi</label>
        <div className="grid grid-cols-4 text-center gap-x-2">
          {props.privateFacility.map((v) => {
            return (
              <div className="w-full">
                {" "}
                <input
                  className="appearance-none"
                  type="checkbox"
                  id={v.id}
                  name="privateFacility"
                  onChange={(e) => {
                    handleChangePrivate(e);
                  }}
                  value={v.name}
                />
                <label style={{ marginBottom: -14 }} className="border-2 cursor-pointer p-0 w-full block" htmlFor={v.id}>
                  {v.name}
                </label>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default Facilities;
