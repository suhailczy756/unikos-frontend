import { Checkbox, Form, Input, message, Select } from "antd";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import UploadImage from "./UploadImage";
import PrimaryButton from "./Buttons/PrimaryButton";
import EditFacility from "./EditFacility";
import RadioGroup from "./RadioGroup";
import axios, { Axios } from "axios";
import { kosRepository } from "../repository/kos";
import Facilities from "./form-create-kos/Facility";
import { set } from "mobx";
import { useRouter } from "next/router";

function FormTambahKos(props) {
  const [name, setName] = useState();
  const [type, setType] = useState();
  const [province, setProvince] = useState();
  const [city, setCity] = useState();
  const [district, setDistrict] = useState();
  const [village, setVillage] = useState();
  const [address, setAddress] = useState();
  const [coordinate, setCoordinate] = useState();
  const [price, setPrice] = useState();
  const [description, setDescription] = useState();
  const [notes, setNotes] = useState();
  const [listFacility, setListFacility] = useState();
  const [listNewFacility, setListNewFacility] = useState();
  const [image, setimage] = useState([]);
  const [privateFacility, setPrivateFacility] = useState();
  const [publicFacility, setPublicFacility] = useState();
  const [listCity, setlistCity] = useState();
  const [listDistrict, setlistDistrict] = useState();
  const [listVillage, setlistVillage] = useState();

  const route = useRouter();

  useEffect(() => {}, [listCity]);

  const { data: Facility } = kosRepository.hooks.getFacility();
  const { data: ListProvince } = kosRepository.hooks.getProvince();

  const getCity = async (id) => {
    const res = await axios.get(`http://localhost:3222/location/city/${id}`);
    setlistCity(res.data);
  };

  const getDistrict = async (id) => {
    const res = await axios.get(
      `http://localhost:3222/location/district/${id}`
    );
    setlistDistrict(res.data);
  };

  const getVillage = async (id) => {
    const res = await axios.get(`http://localhost:3222/location/village/${id}`);
    setlistVillage(res.data);
  };

  const { Option } = Select;
  const { TextArea } = Input;

  const onSubmit = async () => {
    try {
      const datas = {
        name,
        type,
        province_id: province,
        city_id: city,
        district_id: district,
        village_id: village,
        user_id: "",
        address,
        coordinate,
        price,
        description,
        notes,
        listFacility: publicFacility.concat(privateFacility),
        listNewFacility,
        images: image,
      };
      await kosRepository.manipulateData.createKos(datas);
      message
        .loading("Harap Tunggu..", 0.5)
        .then(() => message.success(`Kos Kamu Berhasil Dibuat`, 2.5))
        .then(() => route.push("/my-kos"));
    } catch (e) {
      console.log(e);
    }
  };

  const previousButton = () => {
    props.backStep(20);
  };

  const nextButton = () => {
    props.getStep(20);
  };

  const getType = (value) => {
    setType(value);
  };

  const getImage = async (value) => {
    setimage(value);
  };

  const getPublicFacility = (data) => {
    setPublicFacility(data);
  };

  const getPrivateFacility = (data) => {
    setPrivateFacility(data);
  };

  const getNewFacility = (data) => {
    setListNewFacility(data);
  };

  const handleProvince = async (value) => {
    setProvince(value);
    getCity(value);
  };

  const handleCity = async (value) => {
    setCity(value);
    getDistrict(value);
  };

  const handleDistrict = async (value) => {
    setDistrict(value);
    getVillage(value);
  };

  const handleVillage = async (value) => {
    setVillage(value);
  };

  return (
    <>
      {/* page1 */}
      {props.step === 0 ? (
        <div>
          <div className="mt-8 items-center">
            <label className="w-2/6 block mb-2 font-semibold text-base">
              Nama Kosan
            </label>
            <Form.Item
              rules={[
                {
                  required: true,
                  message: "nama kosan tidak boleh kosong",
                },
              ]}
            >
              <Input
                value={name}
                onChange={(e) => {
                  setName(e.target.value);
                }}
                className="w-6/6 border border-navy"
                name="name"
              />
            </Form.Item>
          </div>
          <div className="mt-8 items-center">
            <label className="w-2/6 block mb-2 font-semibold text-base">
              Deskripsi
            </label>
            <Input
              required
              value={description}
              onChange={(e) => {
                setDescription(e.target.value);
              }}
              className="w-6/6 border border-navy"
              name="description"
            ></Input>
            <span className="w-3/4 text-[0.8rem] block opacity-60">
              Berikan deskripsi yang menarik agar penyewa tertarik
            </span>
          </div>
          <div className="mt-8 items-center">
            <label className="w-2/6 block mb-2 font-semibold text-base">
              Foto Kosan
            </label>
            <sup>Foto yang diupload harus 5 file</sup>
            <UploadImage getImage={getImage} length={5} to={"upkos"} />
            <span className="w-3/4 text-[0.8rem] block opacity-60">
              Gambar yang diupload tidak boleh terbalik, blur, tidak jelas,
              tidak boleh dibuat kolase dan harus berformat JPG atau PNG
            </span>
          </div>
          <div className="mt-10 flex justify-between font-bold items-center space-x-6">
            {/* <Link href="/user">Kembali</Link>
        <div className="w-20">
          <PrimaryButton name={"Lanjut"} />
        </div> */}
            <button
              className="text-blue-500"
              onClick={() => {
                window.history.back(1);
              }}
            >
              Kembali
            </button>
            <PrimaryButton
              name={props.step < 60 ? "Lanjut" : "Konfirmasi"}
              type={"submit"}
              onClick={props.step < 60 ? nextButton : onSubmit}
              disabled={!name || !description || image.length < 5}
            />
          </div>
        </div>
      ) : null}

      {/* page2 */}
      {props.step === 20 ? (
        <div>
          <div className="mt-5 flex items-center">
            <label className="w-2/6 font-semibold text-base">Provinsi</label>
            <Select
              placeholder={"Pilih Provinsi"}
              className="mt-2 border border-navy w-full"
              style={{ display: "block" }}
              onChange={handleProvince}
              value={province}
            >
              {ListProvince?.map((v) => {
                return <Option value={v.id}>{v.name}</Option>;
              })}
            </Select>
          </div>
          <div className="mt-5 flex items-center">
            <label className="w-2/6 font-semibold text-base">Kota</label>
            <Select
              placeholder={"Pilih Kota"}
              className="mt-2 border border-navy w-full"
              style={{ display: "block" }}
              onChange={handleCity}
              value={city}
            >
              {listCity?.map((v) => {
                return <Option value={v.id}>{v.name}</Option>;
              })}
            </Select>
          </div>
          <div className="mt-5 flex items-center">
            <label className="w-2/6 font-semibold text-base">Kecamatan</label>
            <Select
              placeholder={"Pilih Kecamatan"}
              className="mt-2 border border-navy w-full"
              style={{ display: "block" }}
              onChange={handleDistrict}
              value={district}
            >
              {listDistrict?.map((v) => {
                return <Option value={v.id}>{v.name}</Option>;
              })}
            </Select>
          </div>
          <div className="mt-5 flex items-center">
            <label className="w-2/6 font-semibold text-base">Kelurahan</label>
            <Select
              placeholder={"Pilih Kelurahan"}
              className="mt-2 border border-navy w-full"
              value={village}
              style={{ display: "block" }}
              onChange={handleVillage}
            >
              {listVillage?.map((v) => {
                return <Option value={v.id}>{v.name}</Option>;
              })}
            </Select>
          </div>
          <div className="mt-5 flex items-center">
            <label className="w-2/6 font-semibold text-base">Alamat</label>
            <Input
              value={address}
              onChange={(e) => {
                setAddress(e.target.value);
              }}
              className="w-6/6 border border-navy"
              name="name"
            ></Input>
          </div>
          <div className="mt-10 flex justify-end font-bold items-center space-x-6">
            {/* <Link href="/user">Kembali</Link>
        <div className="w-20">
          <PrimaryButton name={"Lanjut"} />
        </div> */}
            <button
              className="text-blue-500"
              hidden={props.step === 0}
              onClick={previousButton}
            >
              Kembali
            </button>
            <PrimaryButton
              name={props.step < 60 ? "Lanjut" : "Konfirmasi"}
              type={"submit"}
              onClick={props.step < 60 ? nextButton : onSubmit}
              disabled={!province || !city || !district || !village || !address}
            />
          </div>
        </div>
      ) : null}

      {/* page3 */}
      {props.step === 40 ? (
        <div>
          <Facilities
            publicFacility={Facility?.publicFacility}
            privateFacility={Facility?.privateFacility}
            getPublicFacility={getPublicFacility}
            getPrivateFacility={getPrivateFacility}
            public={publicFacility}
            private={privateFacility}
          />
          <div className="mt-10 items-center">
            <label className="w-2/6 block mb-2 font-semibold text-base">
              Fasilitas Tambahan
            </label>
            <EditFacility getNewFacility={getNewFacility} />
          </div>
          <div className="mt-10 flex justify-end font-bold items-center space-x-6">
            {/* <Link href="/user">Kembali</Link>
        <div className="w-20">
          <PrimaryButton name={"Lanjut"} />
        </div> */}
            <button
              className="text-blue-500"
              hidden={props.step === 0}
              onClick={previousButton}
            >
              Kembali
            </button>
            <PrimaryButton
              name={props.step < 60 ? "Lanjut" : "Konfirmasi"}
              type={"submit"}
              onClick={props.step < 60 ? nextButton : onSubmit}
              disabled={!name || !description || !image}
            />
          </div>
        </div>
      ) : null}

      {/* page 4 */}
      {props.step === 60 ? (
        <div>
          <div className="mt-5">
            <label className="w-2/6 mb-2 block font-semibold text-base">
              Biaya Kos
            </label>
            <div className="flex items-center">
              <Input
                type={"number"}
                value={price}
                onChange={(e) => {
                  setPrice(e.target.value);
                }}
                className="w-52 border-navy border"
                addonBefore="Rp"
                defaultValue=""
              />
              <span className="ml-2">x 1 bulan</span>
            </div>
          </div>
          <div className="mt-10 items-center">
            <label className="w-2/6 block font-semibold text-base">
              Tipe Kos
            </label>
            <span className="w-3/4 mb-2 text-[0.8rem] block opacity-60">
              untuk siapa kos ini disewakan?
            </span>
            <RadioGroup type={type} getType={getType} />
          </div>
          <div className="mt-10 items-center">
            <label className="w-2/6 block font-semibold text-base">
              Catatan
            </label>
            <span className="w-3/4 mb-2 text-[0.8rem] block opacity-60">
              Boleh berisikan peraturan, tata tertib atau pesan untuk penyewa
              kos (boleh kosong)
            </span>
            <TextArea
              value={notes}
              onChange={(e) => {
                setNotes(e.target.value);
              }}
              showCount
              maxLength={100}
              className="border border-navy"
            />
          </div>
          <div className="mt-10 flex justify-end font-bold items-center space-x-6">
            {/* <Link href="/user">Kembali</Link>
        <div className="w-20">
          <PrimaryButton name={"Lanjut"} />
        </div> */}
            <button
              className="text-blue-500"
              hidden={props.step === 0}
              onClick={previousButton}
            >
              Kembali
            </button>
            <PrimaryButton
              name={props.step < 60 ? "Lanjut" : "Konfirmasi"}
              type={"submit"}
              onClick={props.step < 60 ? nextButton : onSubmit}
              disabled={!name || !description || !image}
            />
          </div>
        </div>
      ) : null}
    </>
  );
}

export default FormTambahKos;
