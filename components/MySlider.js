import React, { useRef, useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import { Pagination } from "swiper";
import MyCard from "./MyCard";
import { useRouter } from "next/router";
import { Tooltip } from "antd";
import { QuestionCircleOutlined } from "@ant-design/icons";

const MySlider = (props) => {
  const route = useRouter();

  const handleClick = (id) => {
    route.push(`/kos/${id}`);
  };

  return (
    <div className="mt-2 mx-auto">
      <div className="w-9/12 mx-auto p-4">
        <div className=" inline">
          <div className="flex items-center">
            <span className="font-bold text-2xl">{props.h2}</span>{" "}
            {props.select == false ? (
              <Tooltip
                title="Kos yang ditampilkan diurutkan berdasarkan rating tertinggi"
                placement="right"
              >
                <span className="ml-2">
                  <QuestionCircleOutlined />
                </span>
              </Tooltip>
            ) : (
              <select
                className="text-xl font-bold ml-2 text-primary active:border-none active:outline-none border-none outline-none cursor-pointer"
                defaultValue={"KOTA BEKASI"}
                style={{
                  border: 0,
                  outline: 0,
                }}
                onChange={(e) => {
                  props.onChange(e.target.value);
                }}
              >
                <option
                  selected
                  value={"KOTA%20BEKASI"}
                  className="w-min px-2 py-4"
                >
                  BEKASI
                </option>
                <option value={"JAKARTA%20PUSAT"} className="w-min px-2 py-4">
                  JAKARTA PUSAT
                </option>
                <option value={"JAKARTA%20BARAT"} className="w-min px-2 py-4">
                  JAKARTA BARAT
                </option>
                <option value={"JAKARTA%20SELATAN"} className="w-min px-2 py-4">
                  JAKARTA SELATAN
                </option>
                <option value={"KOTA%20BOGOR"} className="w-min px-2 py-4">
                  BOGOR
                </option>
                <option value={"KOTA%20DEPOK"} className="w-min px-2 py-4">
                  DEPOK
                </option>
                <option value={"TANGERANG"} className="w-min px-2 py-4">
                  TANGERANG
                </option>
              </select>
            )}
          </div>
        </div>

        <Swiper
          style={{ paddingBottom: "40px" }}
          slidesPerView={3}
          spaceBetween={12}
          pagination={{
            clickable: true,
          }}
          modules={[Pagination]}
          className="mySwiper pt-5"
        >
          {props.highData == undefined ? (
            props.kos && props.kos?.length <= 0 ? (
              <div className=" text-center py-20">
                Kos yang kamu cari tidak tersedia
              </div>
            ) : (
              props.kos?.map((v) => {
                const image = v?.image[0].name;

                return (
                  <SwiperSlide key={v.id}>
                    <MyCard
                      datas={props.datas}
                      onClick={() => {
                        handleClick(v.id);
                      }}
                      name={v.name}
                      status={v.status}
                      image={image}
                      lokasi={v.location}
                      type={v.type}
                      rating={v.rate}
                      price={v.price}
                    />
                  </SwiperSlide>
                );
              })
            )
          ) : (
            props.highData?.data?.map((v) => {
              const image = v?.image[0].name;
              return (
                <SwiperSlide>
                  <MyCard
                    datas={props.datas}
                    onClick={() => {
                      handleClick(v.id);
                    }}
                    name={v.name}
                    status={v.status}
                    image={image}
                    lokasi={v.location}
                    type={v.type}
                    rating={v.rate}
                    price={v.price}
                  />
                </SwiperSlide>
              );
            })
          )}
        </Swiper>
      </div>
    </div>
  );
};

export default MySlider;
