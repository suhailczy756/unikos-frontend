import Image from "next/image";
import { Button, Tooltip } from "antd";

const PendingOwner = () => {
  return (
    <div className="w-10/12 mx-auto py-10">
      <div className="flex justify-between">
        <div className="text-2xl font-semibold">Kosan Saya</div>
        <div>
          <Tooltip placement="left" title="Kamu tidak bisa menambahkan kos baru karena akun kamu belum diverifikasi">
            <Button className="rounded w-full cursor-not-allowed bg-slate-800 p-4 text-base font-bold text-white flex items-center justify-evenly">Tambah Kosan</Button>
          </Tooltip>
        </div>
      </div>
      <div className=" text-center mt-11 text-xl font-bold">
        <div>
          <Image src={"/icons/frown.svg"} width={125} height={125}></Image>
        </div>
        <div>Akun Kamu Belum Di Konfirmasi Oleh Admin</div>
      </div>
    </div>
  );
};

export default PendingOwner;
