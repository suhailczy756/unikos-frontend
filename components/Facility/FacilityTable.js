import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { message, Table } from "antd";
import React, { useRef, useState } from "react";
import { kosRepository } from "../../repository/kos";
import { mutate } from "swr";

const FacilityTable = () => {
  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  const searchInput = useRef(null);
  const { data: allData } = kosRepository.hooks.getAllFacility();

  const handleDelete = async (id) => {
    await kosRepository.manipulateData.deleteFacility(id);
    mutate(kosRepository.url.allFacility());
    message.success("Fasilitas Berhasil Dihapus");
  };

  const columns = [
    {
      title: "No",
      dataIndex: "no",
    },
    {
      title: "Nama fasilitas",
      dataIndex: "nama",
    },
    {
      title: "Tipe Fasilitas",
      dataIndex: "type",
      filters: [
        {
          text: "Fasilitas Umum",
          value: "Fasilitas Umum",
        },
        {
          text: "Fasilitas Pribadi",
          value: "Fasilitas Pribadi",
        },
      ],
      onFilter: (value, record) => record.type.indexOf(value) === 0,
      sortDirections: ["descend"],
    },
    {
      title: "Aksi",
      dataIndex: "aksi",
    },
  ];
  let i = 1;
  const datas = [
    allData?.data.map((v) => {
      return {
        no: i++,
        nama: v.name,
        type: <span className="capitalize">{v.typeof_facility}</span>,
        aksi: (
          <div className="flex space-x-1">
            <button
              onClick={() => {
                handleUpdate(v.id);
              }}
              className="rounded font-bold bg-green-500 text-white flex items-center p-1"
            >
              <EditOutlined />
            </button>
            <button
              onClick={() => {
                handleDelete(v.id);
              }}
              className="rounded font-bold bg-red-500 text-white flex items-center p-1"
            >
              <DeleteOutlined />
            </button>
          </div>
        ),
      };
    }),
  ];

  const onChange = (pagination, filters, sorter, extra) => {
    console.log("params", pagination, filters, sorter, extra);
  };

  return <Table columns={columns} dataSource={datas[0]} onChange={onChange} />;
};

export default FacilityTable;
