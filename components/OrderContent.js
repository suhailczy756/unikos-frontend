import { Divider } from "antd";
import formatCurrency from "../helper/formatCurrency";
import formatDate from "../helper/formatDate";

const OrderContent = (props) => {
  const transaction = props.id;
  return (
    <div className="w-full border px-10 py-16 mx-auto space-y-2">
      <div className="font-semibold flex justify-center text-lg">
        Detail Pesanan
      </div>
      <div className=" flex justify-between">
        <div>Harga Awal</div>
        <div>{formatCurrency(transaction?.boarding?.price)}</div>
      </div>
      <div className=" flex justify-between">
        <div>Tanggal Masuk</div>
        <div>{formatDate(transaction?.start_date)}</div>
      </div>
      <div className=" flex justify-between">
        <div>Tanggal Keluar</div>
        <div>{formatDate(transaction?.end_date)}</div>
      </div>
      <Divider />
      <div className=" flex justify-between font-bold">
        <div>Total Harga</div>
        <div>{formatCurrency(transaction?.price)}</div>
      </div>
    </div>
  );
};

export default OrderContent;
