import { Button, Form, Select } from "antd";
import axios from "axios";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { mutate } from "swr";
import { kosRepository } from "../repository/kos";
const Option = Select;
const onChange = (value) => {
  console.log(`selected ${value}`);
};

const onSearch = (value) => {
  console.log("search:", value);
};

const SelectionSearch = (props) => {
  const [page, setPage] = useState(0);
  const [pageSize, setpageSize] = useState(10);
  const [province, setProvince] = useState();
  const [city, setCity] = useState();
  const [district, setDistrict] = useState();
  const [village, setVillage] = useState();
  const [listCity, setlistCity] = useState();
  const [listDistrict, setlistDistrict] = useState();
  const [listVillage, setlistVillage] = useState();

  const route = useRouter();

  const { data: ListProvince } = kosRepository.hooks.getProvince();

  const { p, c, d } = route.query;

  useEffect(() => {
    if (p || c || d) {
      const prov = p === "undefined" ? undefined : p;
      const cit = c === "undefined" ? undefined : c;
      const dis = d === "undefined" ? undefined : d;
      props.onSearch(page, pageSize, prov, cit, dis, undefined);
    }
  }, [p, c, d]);

  const getCity = async (id) => {
    const res = await axios.get(`http://localhost:3222/location/city/${id}`);
    setlistCity(res.data);
  };

  const getDistrict = async (id) => {
    const res = await axios.get(
      `http://localhost:3222/location/district/${id}`
    );
    setlistDistrict(res.data);
  };

  const handleFinish = () => {
    props.onSearch(page, pageSize, province, city, district, village);
  };

  const getVillage = async (id) => {
    const res = await axios.get(`http://localhost:3222/location/village/${id}`);
    setlistVillage(res.data);
  };

  const handleProvince = async (value) => {
    setProvince(value);
    getCity(value);
  };

  const handleCity = async (value) => {
    setCity(value);
    getDistrict(value);
  };

  const handleDistrict = async (value) => {
    setDistrict(value);
    getVillage(value);
  };

  const handleVillage = async (value) => {
    setVillage(value);
  };

  return (
    <div className="flex justify-between w-10/12 space-x-4">
      <Form className="flex space-x-4" onFinish={handleFinish}>
        <Form.Item
          className="w-full"
          rules={[
            {
              required: true,
              message: "pilih provinsi kosan",
            },
          ]}
        >
          <Select
            className="w-full"
            showSearch
            allowClear
            placeholder="Masukan Nama Provinsi"
            optionFilterProp="children"
            onChange={handleProvince}
            filterOption={(input, option) =>
              option.children.toLowerCase().includes(input.toLowerCase())
            }
          >
            {ListProvince?.map((v) => {
              return <Option value={v.id}>{v.name}</Option>;
            })}
          </Select>
        </Form.Item>
        <Form.Item className="w-full">
          <Select
            className="w-full"
            showSearch
            allowClear
            placeholder="Masukan Nama Kota"
            optionFilterProp="children"
            onChange={handleCity}
            filterOption={(input, option) =>
              option.children.toLowerCase().includes(input.toLowerCase())
            }
          >
            {listCity?.map((v) => {
              return <Option value={v.id}>{v.name}</Option>;
            })}
          </Select>
        </Form.Item>
        <Form.Item className="w-full">
          <Select
            className="w-full"
            showSearch
            allowClear
            placeholder="Masukan Nama Kecamatan"
            optionFilterProp="children"
            onChange={handleDistrict}
            filterOption={(input, option) =>
              option.children.toLowerCase().includes(input.toLowerCase())
            }
          >
            {listDistrict?.map((v) => {
              return <Option value={v.id}>{v.name}</Option>;
            })}
          </Select>
        </Form.Item>
        <Form.Item className="w-full">
          <Select
            className="w-full"
            showSearch
            allowClear
            placeholder="Masukan Nama Kelurahan"
            optionFilterProp="children"
            onChange={handleVillage}
            filterOption={(input, option) =>
              option.children.toLowerCase().includes(input.toLowerCase())
            }
          >
            {listVillage?.map((v) => {
              return <Option value={v.id}>{v.name}</Option>;
            })}
          </Select>
        </Form.Item>
        <Form.Item className="w-full">
          <Button htmlType="submit">Search</Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default SelectionSearch;
