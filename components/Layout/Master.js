import Header from "../Header";
import Footer from "../Footer";
import HeaderNotAuth from "../HeaderNotAuth";
import { useEffect, useState } from "react";
import Head from "next/head";

export default function Master({ children, title }) {
  const [token, setToken] = useState();
  useEffect(() => {
    let token;
    token = localStorage.getItem("token");
    setToken(token);
  }, []);
  return (
    <div>
      {!token ? <HeaderNotAuth /> : <Header />}
      <Head>
        <title>{title}</title>
      </Head>
      <main className="min-h-screen">{children}</main>
      <Footer />
    </div>
  );
}
