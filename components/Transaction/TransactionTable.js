import { Table } from "antd";
import { useState } from "react";
import { TransactionRepository } from "../../repository/transaction";
import SecondaryButton from "../Buttons/SecondaryButton";
import React from "react";
import { useRouter } from "next/router";
import formatDate from "../../helper/formatDate";

const TransactionTable = () => {
  const [page, setPage] = useState(0);
  const [pageSize, setPageSize] = useState(100);
  const { data: allData } = TransactionRepository.hooks.getAllTransaction({
    page,
    pageSize,
  });
  const router = useRouter();

  const goDetail = async (value) => {
    try {
      const id = value;
      router.push(`/transaction/${id}`);
    } catch (e) {
      return e;
    }
  };

  const columns = [
    {
      title: "No",
      dataIndex: "no",
    },
    {
      title: "Nama Kos",
      dataIndex: "nama",
    },
    {
      title: "Penyewa",
      dataIndex: "penyewa",
    },
    {
      title: "Tanggal Masuk",
      dataIndex: "masuk",
    },
    {
      title: "Tanggal Keluar",
      dataIndex: "keluar",
    },
    {
      title: "Status",
      dataIndex: "status",
      filters: [
        {
          text: "Berhasil",
          value: "berhasil",
        },
        {
          text: "Tertunda",
          value: "tertunda",
        },
        {
          text: "Gagal",
          value: "gagal",
        },
        {
          text: "Batal",
          value: "batal",
        },
      ],
      onFilter: (value, record) => record.status.indexOf(value) === 0,
      sortDirections: ["descend"],
    },
    {
      title: "Aksi",
      dataIndex: "aksi",
    },
  ];
  let i = 1;
  const datas = [
    allData?.data?.data.map((v) => {
      return {
        no: i++,
        nama: v.boarding.name,
        penyewa: v.user.name,
        masuk: formatDate(v.start_date),
        keluar: formatDate(v.end_date),
        status: v.status,
        aksi: (
          <SecondaryButton
            value={v.id}
            onClick={(e) => {
              goDetail(e.target.value);
            }}
            name={"Detail"}
          />
        ),
      };
    }),
  ];

  const onChange = (pagination, filters, sorter, extra) => {
    console.log("params", pagination, filters, sorter, extra);
  };

  console.log(datas);

  return <Table dataSource={datas[0]} columns={columns} onChange={onChange} />;
};

export default TransactionTable;
