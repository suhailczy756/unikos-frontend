import { Select } from "antd";
import React, { useState } from "react";
import { TransactionRepository } from "../../repository/transaction";
import TransactionTable from "./TransactionTable";
const Option = Select;

const Content = () => {
  const [page, setPage] = useState(0);
  const [pageSize, setPageSize] = useState(100);
  const { data: allData } = TransactionRepository.hooks.getAllTransaction({
    page,
    pageSize,
  });
  console.log(allData);
  return (
    <div className="mt-2">
      <div className="text-xl font-semibold">List Transaksi</div>

      {allData?.length == 0 ? (
        <div className="rounded-sm shadow-md mx-auto">
          <div className="w-1/2 mx-auto text-center flex flex-col py-16 space-y-3">
            <div className="font-semibold text-lg">Transaksi masih kosong</div>
            <div className="font-medium text-base">Data transaksi semua user akan ditampilkan di halaman ini</div>
          </div>
        </div>
      ) : (
        <TransactionTable />
      )}
    </div>
  );
};
export default Content;
