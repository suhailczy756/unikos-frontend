import Image from "next/image";
import Link from "next/link";
import PrimaryButton from "./Buttons/PrimaryButton";

const HeaderNotAuth = () => {
  return (
    <div className="w-full py-4 px-14 bg-navy flex justify-between font-semibold items-center sticky z-50 top-0">
      <Link href={"/"}>
        <Image src={"/image/logo_navbar.svg"} width="102px" height="40px" className="cursor-pointer" />
      </Link>
      <Link className="" href="/search">
        <span className="text-white cursor-pointer">Pencarian Cepat</span>
      </Link>
      <div>
        <a href={"/login"}>
          <PrimaryButton name="Masuk" />
        </a>
      </div>
    </div>
  );
};

export default HeaderNotAuth;
