import Image from "next/image";
import formatCurrency from "../helper/formatCurrency";

const MyCard = (props) => {
  return (
    <div
      onClick={props.onClick}
      className="capitalize cursor-pointer w-12/12 rounded-md border shadow-md"
    >
      <input type={"hidden"} value={props.value}></input>
      <div className="w-full">
        <img src={props.image} width={400} height={10} />
      </div>
      <div className="p-3">
        <h2 className="font-semibold text-base m-0">{props.name}</h2>
        <h3 className="text-subTitle font-semibold lowercase">
          {props.city}
          {props.lokasi}
        </h3>

        <div className="flex justify-between font-semibold items-center">
          <span className="text-[0.8rem] flex items-center">
            <Image src={"/icons/male.svg"} width={20} height={20} />
            Kos {props.type}
          </span>
          <span className="text-[0.8rem] flex items-center">
            {props.rating == 0 ? null : (
              <div className="flex justify-center">
                <Image src={"/icons/black-star.svg"} width={16} height={16} />
                {props.rating}
              </div>
            )}
          </span>
          <span className="text-base">{formatCurrency(props.price)}</span>
        </div>
      </div>
    </div>
  );
};
export default MyCard;
