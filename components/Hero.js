import { Form, Select } from "antd";
import axios from "axios";
import { useRouter } from "next/router";
import React, { useState } from "react";
import PrimaryButton from "../components/Buttons/PrimaryButton";
import { kosRepository } from "../repository/kos";
const { Option } = Select;

const Hero = () => {
  const handleProvince = (value) => {
    setProvince(value);
    getCity(value);
  };

  const searchProvince = (value) => {
    setProvince(value);
    getCity(value);
  };
  const handleCity = (value) => {
    setCity(value);
    getDistrict(value);
  };

  const searchCity = (value) => {
    setCity(value);
  };
  const handleDistrict = (value) => {
    setDistrict(value);
  };

  const searchDistrict = (value) => {
    setDistrict(value);
  };
  const [province, setProvince] = useState();
  const [city, setCity] = useState();
  const [district, setDistrict] = useState();
  const [listCity, setlistCity] = useState();
  const [listDistrict, setlistDistrict] = useState();
  const { data: ListProvince } = kosRepository.hooks.getProvince();

  const getCity = async (id) => {
    const res = await axios.get(`http://localhost:3222/location/city/${id}`);
    setlistCity(res.data);
  };

  const getDistrict = async (id) => {
    const res = await axios.get(`http://localhost:3222/location/district/${id}`);
    setlistDistrict(res.data);
  };

  const route = useRouter();

  const handleFinish = () => {
    try {
      route.push(`/search?p=${province}&c=${city}&d=${district}`);
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <div>
      <div className="w-full flex bg-navy p-32 justify-between gap-20">
        <div className="max-w-[50%] ">
          <div className="font-bold w-full text-5xl tracking-wide text-white">Cari Kosan Terbaik Dengan Menggunakan Unikos</div>
          <div className="w-full font-semibold text-xl text-white mt-10">Unikos memungkinkan anda mencari kos dengan selera dan lokasi yang sesuai dengan keinginan kamu </div>
        </div>
        <div className="bg-white h-min p-8 rounded-lg">
          <div className="font-bold text-2xl">Cari Kosan Dengan Mudah</div>
          <div className="font-semibold text-xl mt-2 text-subTitle">Masukan informasi dibawah </div>
          <Form onFinish={handleFinish}>
            <div className="mt-7">
              <Select
                className="w-full"
                showSearch
                placeholder="Nama Provinsi"
                optionFilterProp="children"
                onChange={handleProvince}
                onSearch={searchProvince}
                filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
              >
                {ListProvince?.map((v) => {
                  return <Option value={v.id}>{v.name}</Option>;
                })}
              </Select>
            </div>
            <div className="mt-2">
              <Select
                className="w-full"
                showSearch
                placeholder="Nama Kota"
                optionFilterProp="children"
                onChange={handleCity}
                onSearch={searchCity}
                filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
              >
                {listCity?.map((v) => {
                  return <Option value={v.id}>{v.name}</Option>;
                })}
              </Select>
            </div>
            <div className="mt-2">
              <Select
                className="w-full"
                showSearch
                placeholder="Nama Kecamatan"
                optionFilterProp="children"
                onChange={handleDistrict}
                onSearch={searchDistrict}
                filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
              >
                {listDistrict?.map((v) => {
                  return <Option value={v.id}>{v.name}</Option>;
                })}
              </Select>
            </div>
            <div className="mt-2">
              <PrimaryButton name="Cari Kos" />
            </div>
          </Form>
        </div>
      </div>
    </div>
  );
};

export default Hero;
