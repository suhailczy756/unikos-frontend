import Image from "next/image";
import Link from "next/link";
import SecondaryButton from "./Buttons/SecondaryButton";

const Banner = () => {
  return (
    <div className="mx-auto">
      <div className="w-9/12 mx-auto p-4">
        <div className="flex mt-4">
          <div className="w-full">
            <Image src="/image/owner-banner.png" width={450} height={450} />
          </div>
          <div className="w-full p-20 text-black tracking-normal">
            <div className="text-subTitle font-medium">Pemilik Kos</div>
            <div className="font-medium my-2">Jadilah bagian dari unikos dengan mendaftarkan tempat kamu sekarang juga</div>
            <div className="font-normal">Menjadi pemilik kos adalah impian semua orang. Untuk informasi lebih lanjut kamu bisa klik tombol dibawah</div>
            <div className="mt-2 w-1/2">
              <Link href={"/register"}>
                <SecondaryButton icon={<Image src={"/icons/right-arrow.svg"} width={16} height={16} />} name={"Daftar Sekarang"} />
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Banner;
