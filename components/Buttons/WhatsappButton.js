import { WhatsAppOutlined } from "@ant-design/icons";

const WhatsappButton = (props) => {
  return (
    <div className="">
      <a target={"_blank"} href={props.link} className="text-green-500 w-full font-semibold text-lg flex items-center justify-center hover:text-green-600">
        <WhatsAppOutlined className="mr-1" /> Tanya Penjual
      </a>
    </div>
  );
};

export default WhatsappButton;
