const SecondaryButton = (props) => {
  return (
    <button value={props.value} className="rounded w-full  bg-navy p-2 font-bold text-white hover:bg-slate-800 flex items-center justify-evenly" onClick={props.onClick} type={props.type}>
      {props.name}
      {props.icon}
    </button>
  );
};
export default SecondaryButton;
