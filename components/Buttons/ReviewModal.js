import { Form, Input, message, Modal, Rate } from "antd";
import SecondaryButton from "./SecondaryButton";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import PrimaryButton from "./PrimaryButton";
import { kosRepository } from "../../repository/kos";
import { TransactionRepository } from "../../repository/transaction";
import { mutate } from "swr";

const ReviewModal = () => {
  const route = useRouter();
  const { id } = route.query;
  const { data: detailData } = TransactionRepository.hooks.getMyTransactionDetail(id);
  const [boardingId, setBoardingId] = useState("");
  const [loading, setLoading] = useState(true);

  const onFinish = async (values) => {
    setLoading(true);
    try {
      await kosRepository.manipulateData.createReview(boardingId, values);
      message.success("Penilaian Berhasil");
      window.location.reload(false);
    } catch (error) {
      console.log(error);
    } finally {
      setVisible(false);
      setLoading(false);
    }
    mutate(TransactionRepository.url.myTransactionDetail(id));
  };

  useEffect(() => {
    if (detailData) {
      setBoardingId(detailData.boarding.id);
      setLoading(false);
    }
  }, [detailData]);
  const [formLayout, setFormLayout] = useState("vertical");

  const formItemLayout =
    formLayout === "horizontal"
      ? {
          labelCol: {
            span: 4,
          },
          wrapperCol: {
            span: 14,
          },
        }
      : null;
  const [visible, setVisible] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);

  const showModal = () => {
    setVisible(true);
  };

  const handleCancel = () => {
    setVisible(false);
  };
  return (
    <>
      <div className="mb-2">
        <SecondaryButton name={"Beri Penilaian"} onClick={showModal} />
        {loading ? (
          "...loading"
        ) : (
          <Modal footer={false} title="Beri Penilaian" visible={visible} confirmLoading={confirmLoading} onCancel={handleCancel}>
            <p className="bg-yellow-100">Kami menyarankan memberikan nilai pada saat anda selesai menginap di kos pilihan anda !</p>
            <Form
              name="review"
              {...formItemLayout}
              layout={formLayout}
              onFinish={onFinish}
              initialValues={{
                rating: 0,
                layout: formLayout,
              }}
            >
              <Form.Item name="rating" label="">
                <Rate />
              </Form.Item>

              <Form.Item
                name="comment"
                label="Beri Ulasan"
                rules={[
                  {
                    required: true,
                    message: "Ulasan Tidak Boleh Kosong",
                  },
                ]}
              >
                <Input.TextArea showCount maxLength={100} />
              </Form.Item>
              <input type="hidden" value={boardingId} />
              <PrimaryButton name={"Kirim"} />
            </Form>
          </Modal>
        )}
      </div>
    </>
  );
};

export default ReviewModal;
