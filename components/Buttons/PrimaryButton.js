import React from "react";

const PrimaryButton = (props) => {
  return (
    <div>
      <button
        loading={props.loading}
        value={props.value}
        hidden={props.hidden}
        type={props.type}
        className="rounded w-full bg-primary p-2 font-bold text-white hover:bg-slate-500"
        onClick={props.onClick}
        disabled={props.disabled}
      >
        {props.name}
      </button>
    </div>
  );
};
export default PrimaryButton;
