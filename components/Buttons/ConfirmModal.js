import { Modal } from "antd";
import PrimaryButton from "../Buttons/PrimaryButton";
import React, { useState } from "react";

const ConfirmModal = () => {
  const [visible, setVisible] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [modalText, setModalText] = useState("Proses pembayaran akan diproses 1x24 jam. Jika lebih dari jangka waktu tersebut anda berhak membatalkan pesanan");

  const showModal = () => {
    setVisible(true);
  };

  const handleOk = () => {
    setConfirmLoading(true);
    setTimeout(() => {
      setVisible(false);
      setConfirmLoading(false);
    }, 2000);
  };

  const handleCancel = () => {
    setVisible(false);
  };
  return <></>;
};

export default ConfirmModal;
