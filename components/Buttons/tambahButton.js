import { Breadcrumb } from "antd";
import Link from "next/link";
import React from "react";
import SecondaryButton from "./SecondaryButton";

const TambahButton = (props) => {
  return (
    <div className="w-5/6 mx-auto pt-4">
      <Breadcrumb>
        <Breadcrumb.Item>
          <a href="/">Home</a>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Kosan Saya</Breadcrumb.Item>
      </Breadcrumb>
      <div className="flex mt-2 items-center justify-between">
        <h2 className="text-xl font-semibold mb-0">Kosan Saya</h2>
        <div className="w-1/6">
          <Link href={"/my-kos/create-kos"}>
            <SecondaryButton name={"Tambah Kosan"} />
          </Link>
        </div>
      </div>
    </div>
  );
};
export default TambahButton;
