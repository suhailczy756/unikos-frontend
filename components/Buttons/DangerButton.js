import React from "react";

const DangerButton = (props) => {
  return (
    <div>
      <button
        value={props.value}
        onClick={props.onClick}
        className="rounded w-full bg-red-700 p-2 font-bold text-white hover:bg-red-900"
      >
        {props.name}
      </button>
    </div>
  );
};
export default DangerButton;
