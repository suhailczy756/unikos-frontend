import Image from "next/image";

const Welcome = () => {
  return (
    <div className="w-full flex shadow-md border p-4">
      <div className="">
        <Image src={"/image/graph.svg"} width={"72"} height={"72"} />
      </div>
      <div className="flex flex-col">
        <h2 className="font-semibold text-xl px-8">Selamat Datang Admin</h2>
        <p className="px-8 ">Tempat dimana kamu mengelola keseluruhan data kamu</p>
      </div>
    </div>
  );
};

export default Welcome;
