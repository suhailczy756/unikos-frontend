import Link from "next/link";
import UserRepository from "../../repository/user";
import SecondaryButton from "../Buttons/SecondaryButton";

const datas = [
  {
    key: 1,
    name: "Ajis Mutolib",
    phone_numbers: "Fahmi Dahlan",
  },
  {
    key: 2,
    name: "Ajis Mutolib",
    phone_numbers: "Fahmi Dahlan",
  },
  {
    key: 3,
    name: "Ajis Mutolib",
    phone_numbers: "Fahmi Dahlan",
  },
];

const OwnerRequest = () => {
  let i = 1;
  const { data: allData } = UserRepository.hooks.getOwner();
  console.log(allData);
  return (
    <div className="p-5 border shadow-md">
      <h2 className="font-semibold text-lg text-adminContent">
        Daftar Permintaan Pemilik Kos
      </h2>
      {allData?.data[1] == 0 ? (
        <div className="rounded-sm shadow-md mx-auto">
          <div className="w-1/2 mx-auto text-center flex flex-col py-16 space-y-3">
            <div className="font-medium text-base">
              Data permintaan pemilik kos kosong
            </div>
          </div>
        </div>
      ) : (
        <table className="w-full table-auto">
          <thead>
            <tr className="text-adminContent">
              <th>No</th>
              <th>Nama</th>
              <th>Nomor Telepon</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            {allData?.data[0].map((v) => {
              return (
                <tr className="text-center" style={{ padding: "20px" }}>
                  <td style={{ padding: "20px" }}>{i++}</td>
                  <td style={{ padding: "20px" }}>{v.name}</td>
                  <td style={{ padding: "20px" }}>{v.phone_numbers}</td>
                  <td className="flex justify-center pt-5">
                    <span className="w-min">
                      <Link href={"/user"}>
                        <SecondaryButton name="Detail" />
                      </Link>
                    </span>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      )}
    </div>
  );
};

export default OwnerRequest;
