import { RightOutlined } from "@ant-design/icons";
import { Divider } from "antd";
import Image from "next/image";
import Link from "next/link";
import { kosRepository } from "../../repository/kos";

const KosListed = () => {
  const { data: allData } = kosRepository.hooks.getAllKos();

  return (
    <div className="w-full shadow-md border">
      <div className="pt-5 pl-5 flex space-x-2">
        <div className="">
          <Image src={"/image/kos.svg"} width={"64"} height={"64"} />
        </div>
        <div>
          <h2 className="font-semibold text-lg">Kos Terdaftar</h2>
          <p className="text-base">{allData?.count} Kos</p>
        </div>
      </div>
      <Divider style={{ margin: "0" }} />
      <Link href={"/kos"}>
        <div className="text-md p-3 cursor-pointer justify-between flex items-center w-full">
          <div>Lihat Detail</div>
          <RightOutlined />
        </div>
      </Link>
    </div>
  );
};

export default KosListed;
