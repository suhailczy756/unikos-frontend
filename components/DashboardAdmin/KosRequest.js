import Image from "next/image";
import SecondaryButton from "../Buttons/SecondaryButton";
import { kosRepository } from "../../repository/kos";
import { useState } from "react";
import { useRouter } from "next/router";

const KosRequest = () => {
  const [page, setPage] = useState(0);
  const [pageSize, setPageSize] = useState(5);
  const [status, setStatus] = useState("tertunda");
  const { data: allData } = kosRepository.hooks.useRequestKos({
    status,
    page,
    pageSize,
  });

  const route = useRouter();

  const handleClick = async (e) => {
    const id = e.target.value;
    route.push(`/kos/${id}`);
  };
  console.log(allData);
  return (
    <div className="p-5 border shadow-md">
      <h2 className="font-semibold text-lg text-adminContent">Daftar Permintaan Kos</h2>
      {allData?.data?.length == 0 ? (
        <div className="rounded-sm shadow-md mx-auto">
          <div className="w-1/2 mx-auto text-center flex flex-col py-16 space-y-3">
            <div className="font-medium text-base">Daftar permintaan kos belum ada</div>
          </div>
        </div>
      ) : (
        <table className="w-full">
          <thead>
            <tr className="text-adminContent">
              <th>Gambar</th>
              <th>Pemilik</th>
              <th>Status</th>
              <th>Tanggal Pembuatan</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            {allData?.data?.map((v) => {
              const image = v.image[0].name;
              return (
                <tr className="text-center">
                  <td className="flex justify-center items-center">
                    <img className="" src={image} width={"70"} height={"70"} />
                  </td>
                  <td>{v.user?.name}</td>
                  <td>{v.status}</td>
                  <td>{new Date(v.createdAt).toLocaleDateString("id-ID", { dateStyle: "full" })}</td>
                  <td className="w-1/12">
                    <span className="w-min">
                      <SecondaryButton value={v.id} onClick={(e) => handleClick(e)} name="Detail" />
                    </span>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      )}
    </div>
  );
};

export default KosRequest;
