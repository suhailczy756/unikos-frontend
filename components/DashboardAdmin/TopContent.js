import Welcome from "../../components/DashboardAdmin/Welcome";
import KosListed from "../../components/DashboardAdmin/KosListed";
import SuccessTransaction from "../../components/DashboardAdmin/SuccessTransaction";
import RecentActivity from "../../components/DashboardAdmin/RecentActivity";

const TopContent = () => {
  return (
    <div className="flex justify-between space-x-2">
      <div className="w-full flex flex-col">
        <Welcome />
        <div className="flex space-x-1">
          <KosListed />
          <SuccessTransaction />
        </div>
      </div>
      <RecentActivity />
    </div>
  );
};
export default TopContent;
