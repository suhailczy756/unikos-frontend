import { RightOutlined } from "@ant-design/icons";
import { Divider } from "antd";
import Image from "next/image";
import Link from "next/link";
import React, { useState } from "react";
import { TransactionRepository } from "../../repository/transaction";

const SuccessTransaction = () => {
  const [page, setPage] = useState(0);
  const [pageSize, setPageSize] = useState(10);
  const { data: allData } = TransactionRepository.hooks.getAllTransaction({
    page,
    pageSize,
  });
  return (
    <div className="w-full shadow-md border">
      <div className="pt-5 pl-4 flex space-x-2">
        <div className="">
          <Image src={"/image/done.svg"} width={"64"} height={"64"} />
        </div>
        <div>
          <h2 className="font-semibold text-lg">Transaksi Berhasil</h2>
          <p className="text-base">{allData?.data?.count} Transaksi</p>
        </div>
      </div>
      <Divider style={{ margin: "0" }} />
      <Link href={"/transaction"}>
        <div className="text-md p-3 cursor-pointer justify-between flex items-center w-full">
          <div>Lihat Detail</div>
          <RightOutlined />
        </div>
      </Link>
    </div>
  );
};

export default SuccessTransaction;
