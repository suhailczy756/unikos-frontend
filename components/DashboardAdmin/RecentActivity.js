import { Image } from "antd";
import Link from "next/link";
import { useState } from "react";
import { TransactionRepository } from "../../repository/transaction";

const RecentActivity = () => {
  const [page, setPage] = useState(0);
  const [pageSize, setPageSize] = useState(4);
  const { data: allData } = TransactionRepository.hooks.getRecentTransaction({
    page,
    pageSize,
  });
  return (
    <div className="w-full shadow-md border p-4">
      <h2 className="font-semibold text-xl text-[#166A7E]">
        Transaksi Terbaru
      </h2>
      <table className="w-full mt-5">
        <tbody>
          {allData?.map((v) => {
            console.log("sadasdadsas", v);
            return (
              <tr>
                <td>
                  {v?.user?.image == "" ? (
                    <div className="w-8 h-8 flex items-center justify-center text-white font-bold rounded-full bg-blue-800">
                      {v?.user?.name.charAt(0)}
                    </div>
                  ) : (
                    <Image
                      preview={false}
                      src={v?.user?.image}
                      width={25}
                      height={25}
                    />
                  )}
                </td>
                <td>
                  <span className="font-semibold">{v?.user?.name}</span> baru
                  saja melakukan transaksi dengan{" "}
                  <span className="font-semibold">
                    {v?.boarding?.user?.name}
                  </span>
                </td>
                <td>
                  <Link href={`/transaction/${v.id}`}>
                    <span className="text-black cursor-pointer">
                      Lihat Detail
                    </span>
                  </Link>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

export default RecentActivity;
