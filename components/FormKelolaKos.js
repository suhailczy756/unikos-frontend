import { Form, Input, Select } from "antd";
import React, { useState } from "react";
import EditFacility from "./EditFacility";
import RadioGroup from "./RadioGroup";
import UploadImage from "./UploadImage";

const FormKelolaKos = (props) => {
  const [formLayout, setFormLayout] = useState("vertical");

  const onFormLayoutChange = ({ layout }) => {
    setFormLayout(layout);
  };

  const formItemLayout =
    formLayout === "horizontal"
      ? {
          labelCol: {
            span: 4,
          },
          wrapperCol: {
            span: 14,
          },
        }
      : null;

  const { Option } = Select;

  const handleChange = (value) => {};

  return (
    <Form
      {...formItemLayout}
      layout={formLayout}
      // form={form}
      initialValues={{
        layout: formLayout,
      }}
      onValuesChange={onFormLayoutChange}
    >
      <div className="flex justify-between">
        <h1 className="text-2xl font-bold">Kelola Kos</h1>
        <Select defaultValue="tersedia" style={{ width: 120 }} onChange={handleChange}>
          <Option value="terisi">terisi</Option>
          <Option value="tersedia">tersedia</Option>
        </Select>
      </div>
      <div className="mt-3">
        <label className="font-semibold text-base">Nama Kosan</label>
        <Input className="mt-2 border border-navy" name="nama"></Input>
      </div>
      <div className="mt-4">
        <label className="font-semibold text-base">Deskripsi</label>
        <Input className="mt-2 border border-navy" name="nama"></Input>
      </div>
      <div className="flex justify-between mt-4">
        <div className="w-1/2 pr-2">
          <label className="font-semibold text-base">Provinsi</label>
          <Select className="mb-6 mt-2 border border-navy w-full" style={{ display: "block" }} defaultValue="tersedia" onChange={handleChange}>
            <Option value="terisi">terisi</Option>
            <Option value="tersedia">tersedia</Option>
          </Select>

          <label className="font-semibold text-base">Kota</label>
          <Select className="mb-6 mt-2 border border-navy w-full" style={{ display: "block" }} defaultValue="tersedia" onChange={handleChange}>
            <Option value="terisi">terisi</Option>
            <Option value="tersedia">tersedia</Option>
          </Select>
        </div>
        <div className="w-1/2 pl-2">
          <label className="font-semibold text-base">Kecamatan</label>
          <Select className="mb-6 mt-2 border border-navy w-full" style={{ display: "block" }} defaultValue="tersedia" onChange={handleChange}>
            <Option value="terisi">terisi</Option>
            <Option value="tersedia">tersedia</Option>
          </Select>

          <label className="font-semibold text-base">Kode Pos</label>
          <Select className="mt-2 border border-navy w-full" style={{ display: "block" }} defaultValue="tersedia" onChange={handleChange}>
            <Option value="terisi">terisi</Option>
            <Option value="tersedia">tersedia</Option>
          </Select>
        </div>
      </div>
      <div className="">
        <label className="font-semibold text-base block mb-2">Fasilitas</label>
        <EditFacility />
      </div>
      <div className="mt-5">
        <label className="font-semibold text-base block mb-2">Foto Kosan</label>
        <UploadImage />
      </div>
      <div className="mt-3">
        <label className="font-semibold text-base block mb-2">Tipe Kosan</label>
        <RadioGroup />
      </div>
      <div className="mt-5">
        <label className="font-semibold text-base block mb-2">Harga</label>
        <Input className="w-52 border-navy border" addonBefore="Rp" defaultValue="" />
      </div>
      <div className="my-5">
        <label className="font-semibold text-base">Catatan</label>
        <Input className="mt-2 border border-navy" name="nama"></Input>
      </div>
    </Form>
  );
};

export default FormKelolaKos;
