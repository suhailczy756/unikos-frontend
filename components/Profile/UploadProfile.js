import { Button, Modal, Upload } from "antd";
import ImgCrop from "antd-img-crop";
import React, { useEffect, useState } from "react";
import { appConfig } from "../../config/app";
import { imageRepository } from "../../repository/image";

const getBase64 = (file) =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onload = () => resolve(reader.result);

    reader.onerror = (error) => reject(error);
  });

const UploadProfile = (props) => {
  const [previewVisible, setPreviewVisible] = useState(false);
  const [previewImage, setPreviewImage] = useState("");
  const [previewTitle, setPreviewTitle] = useState("");
  const [fileList, setFileList] = useState([]);

  useEffect(() => {
    props.getImage(fileList);
  }, [fileList]);

  const handleCancel = () => setPreviewVisible(false);

  const handlePreview = async (file) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }

    setPreviewImage(file.url || file.preview);
    setPreviewVisible(true);
  };

  const handleChange = async (args) => {
    const file = args.file;

    try {
      const data = { file };
      const uploadProgress = await imageRepository.manipulateData.upKos(file);
      setFileList([
        ...fileList,
        {
          url: appConfig.apiUrl + "/file" + uploadProgress.body.data.filename,
          name: uploadProgress.body.data.filename,
        },
      ]);
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <>
      <ImgCrop rotate>
        <Upload
          onPreview={handlePreview}
          customRequest={(args) => handleChange(args)}
          name="image"
          fileList={fileList}
          showUploadList={false}
          maxCount={1}
        >
          <Button
            onClick={() => {
              setFileList([]);
            }}
            className="rounded w-full  bg-navy p-2 font-bold text-white hover:bg-slate-800 flex items-center justify-evenly"
          >
            Ubah Foto
          </Button>
        </Upload>
      </ImgCrop>
      <Modal
        visible={previewVisible}
        title={previewTitle}
        footer={null}
        onCancel={handleCancel}
      >
        <img
          alt="example"
          style={{
            width: "100%",
          }}
          src={previewImage}
        />
      </Modal>
    </>
  );
};

export default UploadProfile;
