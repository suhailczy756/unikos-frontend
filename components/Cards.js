import { Tag } from "antd";
import React from "react";
import formatDate from "../helper/formatDate";

const Cards = (props) => {
  return (
    <div className="w-4/12">
      <div className="cards ml-10 mt-20 w-full border shadow-md p-3 pt-5 gap-2 pl-8 pr-8 rounded-md">
        <div className="flex justify-between">
          <div className="w-3/4">
            <span className="w-full text-lg font-bold block break-words">{props.name}</span>
            <span className="w-full text-md opacity-50 font-bold block break-words capitalize">Kosan - {props.type}</span>
          </div>
          <div className="w-1/4 pl-4">
            {props.status == "terisi" ? (
              <Tag color={"#1FC1C3"} className="">
                {props?.status}
              </Tag>
            ) : null}
            {props.status == "tersedia" ? (
              <Tag color={"#87d068"} className="">
                {props?.status}
              </Tag>
            ) : null}
          </div>
        </div>
        <div className="w-full text-[0.8rem] mt-2 font-light break-words">{props.description}</div>
        <h3 className="text-base mt-3 font-semibold">Fasilitas</h3>
        <div className="my-1 grid grid-cols-3 gap-x-3 break-words items-center">
          {props.facilities?.map((v) => {
            return <h4 className="font-normal text-sm">{v}</h4>;
          })}
        </div>
        <div className="flex justify-between">
          <div>
            <h3 className="text-base font-semibold mb-0">Dibuat pada</h3>
            <h4 className="font-normal text-sm">{formatDate(props.createdAt)}</h4>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Cards;
