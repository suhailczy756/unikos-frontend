import { Button, Form, Input, Radio } from "antd";

const InputForm = (props) => {
  return (
    <div className="mb-3">
      <Form.Item className="text-title font-normal">
        <Input onChange={props.onChange} type={props.type} name={props.name} className=" p-2 text-[0.7rem] border border-navy rounded w-72" placeholder={`Masukan ${props.label} Kamu`} />
      </Form.Item>
    </div>
  );
};

export default InputForm;
