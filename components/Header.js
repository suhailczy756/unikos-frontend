import Image from "next/image";
import Link from "next/link";
import { DownOutlined } from "@ant-design/icons";
import { Dropdown, Menu, Space } from "antd";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { parseJwt } from "../helper/parseJwt";
import UserRepository from "../repository/user";

const kos = (
  <Menu
    items={[
      {
        label: <Link href="/kos">List Kos</Link>,
        key: "0",
      },
      {
        label: <Link href="/facility">List Fasilitas</Link>,
        key: "1",
      },
      {
        label: <Link href="/approval-kos">Permintaan Kos</Link>,
        key: "2",
      },
    ]}
  />
);

const Header = () => {
  const router = useRouter();
  const [user, setUser] = useState();

  const { data: Profile } = UserRepository.hooks.getProfile();

  useEffect(() => {
    let token;
    if (typeof window !== "undefined") {
      token = localStorage.getItem("token");
    }
    const dataJwt = parseJwt(token);
    setUser(dataJwt?.existUser);
  }, []);

  const handleLogout = () => {
    localStorage.removeItem("token");

    window.location.reload(false);
  };

  const auth = (
    <Menu
      items={[
        {
          label: <Link href={`/profile`}>Pengaturan Akun</Link>,
          key: "0",
        },
        {
          type: "divider",
        },
        {
          label: <span onClick={handleLogout}>Logout</span>,
          key: "1",
          onClick: handleLogout,
        },
      ]}
    />
  );
  return (
    <div className="w-full py-4 px-14 bg-navy flex justify-between font-semibold items-center sticky z-50 top-0">
      <div className="">
        <Link href="/">
          <Image src={"/image/logo_navbar.svg"} width="102px" height="40px" className="cursor-pointer" />
        </Link>
      </div>
      <div className="flex space-x-4 justify-center">
        {user?.role === "admin" ? (
          <>
            <Link className="" href="/dashboard">
              <span className="text-white cursor-pointer">Dashboard</span>
            </Link>
            <Link className="" href="/transaction">
              <span className="text-white cursor-pointer">Transaksi</span>
            </Link>
            <Link href={"/user"}>
              <div className="cursor-pointer text-white">Pengguna</div>
            </Link>
            <Dropdown overlay={kos}>
              <a onClick={(e) => e.preventDefault()}>
                <Space>
                  <div className="cursor-pointer text-white">Kos</div>
                </Space>
              </a>
            </Dropdown>
          </>
        ) : null}

        {user?.role === "owner" ? (
          <Link className="" href="/my-kos">
            <span className="text-white cursor-pointer">Kos Saya</span>
          </Link>
        ) : null}

        {user?.role === "owner" ? (
          <Link className="" href="/my-transaction">
            <span className="text-white cursor-pointer">Transaksi</span>
          </Link>
        ) : null}

        {user?.role === "client" ? (
          <Link className="" href="/search">
            <span className="text-white cursor-pointer">Pencarian Cepat</span>
          </Link>
        ) : null}
        {user?.role === "client" ? (
          <Link className="" href="/order">
            <span className="text-white cursor-pointer">Pesanan Saya</span>
          </Link>
        ) : null}
      </div>
      <div className="flex space-x-2 items-center justify-center">
        {Profile?.image == "" ? (
          <div className="w-8 h-8 flex items-center justify-center text-white font-bold rounded-full bg-blue-800">{user?.name.charAt(0)}</div>
        ) : (
          <div>
            <img style={{ height: "40px", width: "40px" }} className="rounded-[50%]" src={Profile?.image} />
          </div>
        )}
        <div>
          <Dropdown overlay={auth} trigger={["click"]}>
            <a onClick={(e) => e.preventDefault()}>
              <Space>
                <div className="text-white">
                  {user?.name}
                  <DownOutlined className="pl-2" />
                </div>
              </Space>
            </a>
          </Dropdown>
        </div>
      </div>
    </div>
  );
};

export default Header;
