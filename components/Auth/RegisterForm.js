import { Form, Input, Radio, message } from "antd";
import Link from "next/link";
import { useState } from "react";
import PrimaryButton from "../Buttons/PrimaryButton";
import { useRouter } from "next/router";
import UserRepository from "../../repository/user";

const RegisterForm = () => {
  const [name, setNameUser] = useState("");
  const [phone_numbers, setPhoneNumbers] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [role_id, setRoleId] = useState("");
  const [formLayout, setFormLayout] = useState("vertical");
  const router = useRouter();

  const handleClick = async () => {
    try {
      const data = {
        name,
        phone_numbers: `62${phone_numbers}`,
        email,
        password,
        role_name: role_id,
      };
      message
        .loading("Harap Tunggu..", 0.5)
        .then(() => UserRepository.manipulateData.postRegister(data))
        .then(() => router.push("/login"))
        .then(() => message.success(`Akun Berhasil Dibuat`, 2.5));
    } catch (e) {
      console.log(e);
    }
  };

  const onFormLayoutChange = ({ layout }) => {
    setFormLayout(layout);
  };
  const formItemLayout =
    formLayout === "horizontal"
      ? {
          labelCol: {
            span: 4,
          },
          wrapperCol: {
            span: 14,
          },
        }
      : null;
  return (
    <div>
      <Form
        {...formItemLayout}
        onFinish={handleClick}
        layout={formLayout}
        initialValues={{
          layout: formLayout,
        }}
        onValuesChange={onFormLayoutChange}
      >
        <div className="mb-2">
          <span className="text-sm font-semibold">Nama</span>
          <Form.Item
            name="nama"
            rules={[
              {
                max: 16,
                message: "Karakter tidak boleh lebih dari 16!",
              },
              {
                required: true,
                message: "Harap masukan nama anda!",
              },
            ]}
          >
            <Input
              name=""
              placeholder="Masukan nama anda"
              value={name}
              onChange={(e) => setNameUser(e.target.value)}
              className=" w-72 border border-navy rounded"
            />
          </Form.Item>
        </div>
        <div className="">
          <span className="text-sm font-semibold">Email</span>
          <Form.Item
            name="email"
            rules={[
              {
                type: "email",
                message: "Ini bukan format email yang valid!",
              },
              {
                required: true,
                message: "Harap masukan email anda!",
              },
            ]}
          >
            <Input
              placeholder="Masukan email anda"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              className=" w-72 border border-navy rounded"
            />
          </Form.Item>
        </div>
        <div className="mb-5">
          <span className="text-sm font-semibold">Sandi</span>
          <Form.Item
            name={"password"}
            rules={[
              {
                required: true,
                message: "Harap masukan sandi anda",
              },
            ]}
          >
            <Input.Password
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              className="pl-2 w-72 border border-navy rounded"
              label="Sandi"
              type={"password"}
              placeholder={"Masukan Sandi Kamu"}
            />
          </Form.Item>
        </div>
        <div className="">
          <span className="text-sm font-semibold">Nomor Telepon</span>
          <Form.Item
            name={"phone_numbers"}
            rules={[
              {
                required: true,
                message: "Harap masukan nomor telepon anda",
              },
              {
                max: 14,
                message: "Nomor telepon tidak valid!",
              },
              {
                min: 10,
                message: "Nomor telepon tidak valid",
              },
            ]}
          >
            <Input
              prefix="+62"
              value={phone_numbers}
              type={"number"}
              onChange={(e) => setPhoneNumbers(e.target.value)}
              className="pl-2 w-72 border border-navy rounded"
              placeholder={"Masukan nomor telepon kamu"}
            />
          </Form.Item>
        </div>
        <div className="mb-5">
          <span className="text-sm font-semibold">Pilih level</span>
          <div className="">
            <Form.Item
              name={"role"}
              rules={[
                {
                  required: true,
                  message: "Pilih role anda!",
                },
              ]}
            >
              <Radio.Group
                className="space-x-4"
                onChange={(e) => setRoleId(e.target.value)}
              >
                <Radio.Button value="owner">Pemilik</Radio.Button>
                <Radio.Button value="client">Penyewa</Radio.Button>
              </Radio.Group>
            </Form.Item>
          </div>
        </div>
        <div className="mb-3">
          <PrimaryButton type="submit" name="Daftar" />
          <label
            className="font-normal text-xs w-72 flex justify-center mt-2 text-navy"
            href="/login"
          >
            Saya Sudah Punya Akun?
            <Link href={"/login"}>
              <span className="text-primary cursor-pointer ml-1"> Masuk</span>
            </Link>
          </label>
        </div>
      </Form>
    </div>
  );
};

export default RegisterForm;
