import Image from "next/image";

const Content = () => {
  return (
    <div className="mx-auto bg-mainContent">
      <div className="w-9/12 mx-auto p-20">
        <div className="text-center">
          <div className="font-semibold text-3xl">
            <span className="text-[#166A7E]">Mengapa </span> <span className="text-navy"> Unikos ?</span>
          </div>
          <div className="font-semibold text-2xls">Unikos memungkinkan kamu untuk </div>
        </div>
        <div className="flex justify-around mt-4">
          <div className="w-72 text-center">
            <div className="">
              <Image src={"/icons/chat-icon.svg"} width={100} height={100} />
            </div>
            <div className="font-medium text-2xl">Live Chat</div>
            <div className="font-normal text-base">Kemudahan untuk berkomunikasi antar pemilik kos dan penyewa.</div>
          </div>
          <div className="w-72 text-center">
            <div>
              <Image src={"/icons/lock-icon.svg"} width={100} height={100} />
            </div>
            <div className="font-medium text-2xl">Keamanan Terjamin</div>
            <div className="font-normal text-base">Menjamin keamanan proses transaksi.</div>
          </div>
          <div className="w-72 text-center">
            <div>
              <Image src={"/icons/gps-icon.svg"} width={100} height={100} />
            </div>
            <div className="font-medium text-2xl">Lokasi Akurat</div>
            <div className="font-normal text-base">Menyediakan kosan dengan lokasi yang sesuai.</div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Content;
