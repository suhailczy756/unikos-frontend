import { Input, Radio, Space } from "antd";
import React, { useEffect, useState } from "react";

const RadioGroup = (props) => {
  const [value, setValue] = useState(1);
  const [type, setType] = useState(props?.type);

  const onChange = (e) => {
    setValue(e.target.value);
    props.getType(e.target.value);
  };

  return (
    <Radio.Group onChange={onChange} defaultValue={type}>
      <Space direction="vertical">
        <Radio value={"pria"}>Pria</Radio>
        <Radio value={"wanita"}>Wanita</Radio>
        <Radio value={"campuran"}>Campuran</Radio>
      </Space>
    </Radio.Group>
  );
};

export default RadioGroup;
