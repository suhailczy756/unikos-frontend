import { Table, Tag } from "antd";
import React, { useRef, useState } from "react";
import UserRepository from "../../repository/user";
import SecondaryButton from "../Buttons/SecondaryButton";
import { useRouter } from "next/router";

const UserTable = () => {
  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  const searchInput = useRef(null);

  const router = useRouter();

  const goDetail = async (e) => {
    try {
      const id = e.target.value;
      await router.push(`user/${id}`);
    } catch (e) {
      console.log(e);
    }
  };

  const { data: allData } = UserRepository.hooks.getActiveUser({});

  const columns = [
    {
      title: "No",
      dataIndex: "no",
    },
    {
      title: "Nama Pengguna",
      dataIndex: "nama",
    },
    {
      title: "Status",
      dataIndex: "status",
      sortDirections: ["descend"],
    },

    {
      title: "Email",
      dataIndex: "email",
    },
    {
      title: "Level",
      dataIndex: "level",
      filters: [
        {
          text: "client",
          value: "client",
        },
        {
          text: "owner",
          value: "owner",
        },
      ],
      onFilter: (value, record) => record.level.indexOf(value) === 0,
      sortDirections: ["descend"],
    },
    {
      title: "Aksi",
      dataIndex: "aksi",
    },
  ];
  let x = 1;
  const datas = [
    allData?.data?.map((v) => {
      return {
        no: x++,
        nama: v.name,
        status: <Tag color={"#87d068"}>{v.status}</Tag>,
        email: v.email,
        level: v.role.name,
        aksi: <SecondaryButton onClick={goDetail} value={v.id} name={"Detail"} />,
      };
    }),
  ];

  const onChange = (pagination, filters, sorter, extra) => {
    console.log("params", pagination, filters, sorter, extra);
  };

  return (
    <>
      <Table columns={columns} dataSource={datas[0]} onChange={onChange} />
    </>
  );
};
export default UserTable;
