const Footer = () => {
  return <div className="w-full py-4 px-14 bg-footer flex justify-center font-semibold items-center text-white">Unikos © 2022</div>;
};
export default Footer;
