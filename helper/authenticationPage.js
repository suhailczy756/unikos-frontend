import { useRouter } from "next/router";

const authenticationPage = () => {
  const router = useRouter();
  if (typeof window !== "undefined") {
    if (!localStorage.getItem("token")) {
      window.location.href = "/login";
    }
  }
};
export default authenticationPage;
