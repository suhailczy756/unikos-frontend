const formatDate = (date) => {
  return new Date(date).toLocaleDateString("id-ID", { dateStyle: "full" });
};
export default formatDate;
